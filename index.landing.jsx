import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

// landing assets
// require('./assets/fonts/PermianSansTypeface.otf');
require('./assets/landing/css/custom-animations.css');
require('./assets/landing/css/lib/font-awesome.min.css');
require('./assets/landing/css/style.css');
require('./assets/landing/style.scss');

// window.jQuery = window.$ = require('./assets/landing/js/jquery-2.1.4.min.js?ver=1');
// window.RB = require('react-bootstrap');

require('./assets/landing/js/bootstrap.min.js');
require('./assets/landing/js/jquery.flexslider-min.js');
require('./assets/landing/js/jquery.appear.js');
require('./assets/landing/js/jquery.plugin.js');
require('./assets/landing/js/jquery.countdown.js');
require('./assets/landing/js/jquery.waypoints.min.js');
require('./assets/landing/js/jquery.validate.min.js');
require('./assets/landing/js/toastr.min.js');
require('./assets/landing/js/startuply.js');

require('./assets/landing/react');
