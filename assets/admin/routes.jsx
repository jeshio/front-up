import React from 'react';
import { Route, Switch } from 'react-router-dom';
import App from './core/containers/App';
import NotFound from './core/components/NotFound';
import Projects from './Project';
import { LoginPage } from './User';


const routes = (
  <App>
    <Switch>
      <Route path="/" exact component={NotFound} />
      <Route path="/projects" component={Projects} />
      <Route path="/login" component={LoginPage} />
      <Route path="*" component={NotFound} />
    </Switch>
  </App>
);

export default routes;
