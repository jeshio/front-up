import { combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr';
import user from '../../User/reducers';
import projects from '../../Project/reducers';

const rootReducer = combineReducers({
  user,
  projects,
  toastr: toastrReducer,
});

export default rootReducer;
