import React from 'react';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import { Link } from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr';

const { SubMenu } = Menu;
const { Content, Sider, Footer } = Layout;

const App = function App(props) {
  return (
    <Layout style={{ minHeight: '100%' }}>
      <ReduxToastr
        timeOut={4000}
        newestOnTop={false}
        preventDuplicates
        position="top-left"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar
      />
      <Sider width={200}>
        <Menu
          mode="inline"
          defaultOpenKeys={['sub1']}
          theme="dark"
        >
          <Menu.Item key="root">
            <Link to="/">Главная</Link>
          </Menu.Item>
          <SubMenu key="sub1" title={<span><Icon type="user" />Проекты</span>}>
            <Menu.Item key="1"><Link to="/projects">Каталог</Link></Menu.Item>
            <Menu.Item key="2"><Link to="/projects/add">Добавить</Link></Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" title={<span><Icon type="user" />Пользователь</span>}>
            <Menu.Item key="3"><Link to="/">Admin</Link></Menu.Item>
            <Menu.Item key="4"><Link to="/login">Login</Link></Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout style={{ padding: '0 24px 24px' }}>
        <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 280 }}>
          {props.children}
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          ©2017 Front-Up
        </Footer>
      </Layout>
    </Layout>
  );
};

App.propTypes = {
  children: React.PropTypes.object.isRequired,
};

export default App;
