import 'antd/dist/antd.css';
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import routes from './routes';
import configureStore from './configureStore';
import './style.scss';


const store = configureStore();

render(
  <Provider store={store}>
    <BrowserRouter basename="/admin">
      {routes}
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
);
