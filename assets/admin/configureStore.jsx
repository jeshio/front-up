import thunk from 'redux-thunk';
import { Iterable } from 'immutable';
import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import rootReducer from './core/reducers';

export default function configureStore() {
  const middlewares = [thunk];

  if (process.env.NODE_ENV === 'development') {
    const logger = createLogger({
      // diff: true,
      // приспосабливаем к immutable
      stateTransformer: state =>
        (Iterable.isIterable(state) ? state.toJS() : state),
    });
    middlewares.push(logger);
  }

  const store = createStore(
    rootReducer,
    applyMiddleware(...middlewares),
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./core/reducers', () => {
      const nextRootReducer = require('./core/reducers').rootReducer;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
