import { API_PATH } from '../../core/constants';

export const PATH_PROJECTS_API = `${API_PATH}projects/`;
export const PATH_PROJECTS = '/projects/';
