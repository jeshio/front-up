import { fromJS } from 'immutable';
import * as actionTypes from '../actionTypes';

const initJS = {
  isFetching: false,
  items: [],
  project: {},
};

const init = fromJS(initJS);

const reducers = (state = init, action) => {
  switch (action.type) {
  case actionTypes.REQUEST_PROJECT:
    return state
      .set('project', initJS.project)
      .set('isFetching', true);
  case actionTypes.REQUEST_PROJECTS:
    return state
      .set('items', initJS.items)
      .set('isFetching', true);
  case actionTypes.RECEIVE_PROJECT:
    return state
      .set('isFetching', initJS.isFetching)
      .set('project', action.project)
      .set('lastUpdated', action.receivedAt);
  case actionTypes.RECEIVE_PROJECTS:
    return state
      .set('isFetching', initJS.isFetching)
      .set('items', action.projects)
      .set('lastUpdated', action.receivedAt);
  case actionTypes.REQUEST_PROJECT_UPDATE_ERROR:
  case actionTypes.REQUEST_PROJECT_CREATE_ERROR:
  case actionTypes.REQUEST_PROJECT_DELETE_ERROR:
    return state
      .set('isFetching', false);
  // case actionTypes.REQUEST_PROJECT_UPDATE:
  case actionTypes.REQUEST_PROJECT_CREATE:
  case actionTypes.REQUEST_PROJECT_DELETE:
    return state
      .set('project', action.project)
      .set('isFetching', true);
  // case actionTypes.REQUEST_PROJECT_UPDATE_SUCCESS:
  case actionTypes.REQUEST_PROJECT_CREATE_SUCCESS:
    return state
      .set('isFetching', initJS.isFetching)
      .set('project', action.project);
  case actionTypes.REQUEST_PROJECT_DELETE_SUCCESS: {
    const projects = state.get('items');
    return state
      .set('isFetching', initJS.isFetching)
      .set('items', projects.filter(p => p.id !== action.id));
  }
  default:
    return state;
  }
};

export default reducers;
