import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Row, Col, Icon } from 'antd';
import { withRouter } from 'react-router-dom';
import { Table } from '../components';
import { fetchProjects, deleteProject, upProjectPriority, downProjectPriority } from '../actions';

class List extends Component {
  componentDidMount() {
    const { dispatch, items } = this.props;
    if (items.length === 0) {
      dispatch(fetchProjects());
    }
  }

  handleRefreshClick = e => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(fetchProjects());
  }

  render() {
    return (
      <Row>
        <Col span={24}>
          <div className="table-operations">
            <Button type="primary" onClick={() => this.props.history.push('/projects/add')}>Добавить</Button>
            <Button onClick={this.handleRefreshClick} loading={this.props.isFetching}>
              {this.props.isFetching ? '' : (<span><Icon type="sync" /> Обновить</span>)}
            </Button>
          </div>
          <Table
            projects={this.props.items}
            onDelete={project => this.props.dispatch(deleteProject(project))}
            onUpPriority={project => this.props.dispatch(upProjectPriority(project))}
            onDownPriority={project => this.props.dispatch(downProjectPriority(project))}
            loading={this.props.isFetching}
          />
        </Col>
      </Row>
    );
  }
}

List.propTypes = {
  history: React.PropTypes.object.isRequired,
  items: React.PropTypes.array.isRequired,
  isFetching: React.PropTypes.bool.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = state => (state.projects.toJS());

export default withRouter(connect(mapStateToProps)(List));
