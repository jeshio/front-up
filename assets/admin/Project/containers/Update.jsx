import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import { withRouter } from 'react-router-dom';
import { Form } from '../components';
import { updateProject, fetchProject } from '../actions';

class Update extends Component {
  componentDidMount() {
    const { dispatch, match } = this.props;

    const { params } = match;
    dispatch(fetchProject(params.id));
  }

  render() {
    const { project } = this.props;

    return (
      <div>
        <Row type="flex">
          <Col span={24}>
            <h1>Изменение проекта &quot;{project.title}&quot;</h1>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form
              {...this.props}
              onSubmit={values => this.props.dispatch(updateProject(
                { ...project, ...values },
                json => this.props.history.push(`/projects/${json.id}`),
              ))}
              project={project}
              submitTitle="Обновить"
            />
          </Col>
        </Row>
      </div>
    );
  }
}

Update.propTypes = {
  project: React.PropTypes.object.isRequired,
  history: React.PropTypes.object.isRequired,
  match: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = state => state.projects.toJS();

export default withRouter(connect(mapStateToProps)(Update));
