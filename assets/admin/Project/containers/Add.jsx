import React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import { withRouter } from 'react-router-dom';
import { Form } from '../components';
import { createProject } from '../actions';

const Add = function (props) {
  return (
    <div>
      <Row type="flex">
        <Col span={24}>
          <h1>Добавить проект</h1>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Form
            onSubmit={project => props.dispatch(createProject(
              project,
              json => props.history.push(`/projects/${json.id}`),
            ))}
            project={props.project}
            creating
          />
        </Col>
      </Row>
    </div>
  );
};

Add.propTypes = {
  history: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  project: React.PropTypes.object.isRequired,
};

const mapStateToProps = state => state.projects.toJS();

export default withRouter(connect(mapStateToProps)(Add));
