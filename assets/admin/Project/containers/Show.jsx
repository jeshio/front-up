import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Spin, Row, Col, Table, Button } from 'antd';
import { withRouter } from 'react-router-dom';
import { PATH_PROJECTS } from '../constants';
import { fetchProject } from '../actions';

class Show extends Component {
  componentDidMount() {
    const { dispatch, match } = this.props;

    const { params } = match;
    dispatch(fetchProject(params.id));
  }

  getTable() {
    const { project } = this.props;

    const columns = [
      {
        dataIndex: 'title',
        key: 'title',
      },
      {
        dataIndex: 'value',
        key: 'value',
      },
    ];

    const data = [
      {
        key: '1',
        title: 'Статус',
        value: project.public ? 'Опубликовано' : 'Не опубликовано',
      },
      {
        key: '2',
        title: 'Срок реализации',
        value: project.deadline,
      },
      {
        key: '3',
        title: 'Обновлено',
        value: project.updated_at,
      },
    ];

    return (
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        showHeader={false}
      />
    );
  }

  render() {
    const logoStyle = {
      width: '100%',
      padding: '10px',
    };

    const { project } = this.props;

    return (
      <Spin spinning={this.props.isFetching}>
        <Row type="flex" justify="end">
          <Col md={6}>
            <img src={project.logo_path} style={logoStyle} alt="превью проекта" />
          </Col>
          <Col md={18}>
            <Row type="flex" justify="end">
              <Col span={24}>
                <h1>{project.title || 'Загрузка проекта...'}</h1>
                <h2>{project.subtitle}</h2>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                {this.getTable()}
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Row>
            <Col span={24}>
              <h3>Описание</h3>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <p>
                {project.description || 'загружается...'}
              </p>
            </Col>
          </Row>
        </Row>
        <Row>
          <Col span={24}>
            <p style={{ padding: '10px 0' }}>
              <Button type="primary" onClick={() => this.props.history.push(`${PATH_PROJECTS}update/${project.id}`)}>Изменить</Button>
            </p>
          </Col>
        </Row>
      </Spin>
    );
  }
}

Show.propTypes = {
  history: React.PropTypes.object.isRequired,
  project: React.PropTypes.object.isRequired,
  match: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  isFetching: React.PropTypes.bool.isRequired,
};

const mapStateToProps = state => state.projects.toJS();

export default withRouter(connect(mapStateToProps)(Show));
