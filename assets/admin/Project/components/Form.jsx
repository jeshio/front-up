import React from 'react';
import {
  Form as AntForm, Input, Switch,
  Button, Upload, Icon, Spin,
} from 'antd';

const FormItem = AntForm.Item;
const { TextArea } = Input;

const Form = function (props) {
  const { getFieldDecorator, setFieldsValue } = props.form;
  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
  };

  const { onSubmit, project } = props;


  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      console.log('handleSubmit', values);
      let sendData = { ...values };

      try {
        const logoValue = values.logo[0];

        const { url, extension } = logoValue;

        sendData = { ...sendData, logo: url, extension };
      } catch (e) {

      }

      if (!err) {
        console.log('values', sendData);
        onSubmit(sendData);
      }
    });
  };

  const normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const handleEvent = e => {
    console.log('handleEvent', e);
    e.preventDefault();
    if (!e || !e.fileList) {
      return e;
    }

    const { fileList } = e;
    return fileList;
  };

  const handleLogoUpload = logoFile => {
    console.log(logoFile);
    const reader = new FileReader();
    reader.readAsDataURL(logoFile);

    const extension = /(?:\.([^.]+))?$/.exec(logoFile.name)[1];

    reader.onloadend = () => {
      console.log('onloadend', reader, extension);
      // console.log(reader.result);
      setFieldsValue({
        logo: [{
          uid: -1,
          status: 'done',
          url: reader.result,
          extension,
        }],
      });
    };

    return false;
  };

  let defaultLogo = [];

  if (project && project.logo_path) {
    const name = /([^/]+\..+)$/.exec(project.logo_path)[0];
    defaultLogo = [{
      uid: -1,
      status: 'done',
      url: project.logo_base64,
      extension: project.extension,
    }];
    console.log('defaultLogo', defaultLogo);
  }

  return (
    <Spin spinning={props.isFetching}>
      <AntForm onSubmit={handleSubmit}>
        <FormItem
          {...formItemLayout}
          label="Логотип"
        >
          {getFieldDecorator('logo', {
            valuePropName: 'fileList',
            getValueFromEvent: handleEvent,
            normalize: normFile,
            rules: [{ required: props.creating }],
            initialValue: defaultLogo,
          })(
            <Upload
              name="logo"
              action="/projects"
              listType="picture"
              beforeUpload={handleLogoUpload}
            >
              <Button>
                <Icon type="upload" /> Нажмите, чтобы загрузить
              </Button>
            </Upload>,
          )}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Заголовок"
        >
          {getFieldDecorator('title', {
            rules: [{ required: true }],
          })(<Input />)}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Подзаголовок"
        >
          {getFieldDecorator('subtitle')(<Input />)}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Описание"
        >
          {getFieldDecorator('description', {
            rules: [{ required: true }],
          })(<TextArea rows={6} />)}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Срок выполнения"
        >
          {getFieldDecorator('deadline', {
            rules: [{ required: true }],
          })(<Input />)}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="Опубликовать"
        >
          {getFieldDecorator('public', { valuePropName: 'checked', initialValue: true })(
            <Switch />,
          )}
        </FormItem>

        <FormItem
          wrapperCol={{ span: 12, offset: 6 }}
        >
          <Button type="primary" htmlType="submit">{props.submitTitle}</Button>
        </FormItem>
      </AntForm>
    </Spin>
  );
};

Form.defaultProps = {
  submitTitle: 'Добавить',
  isFetching: false,
  creating: false,
};

Form.propTypes = {
  submitTitle: React.PropTypes.string,
  form: React.PropTypes.object.isRequired,
  onSubmit: React.PropTypes.func.isRequired,
  isFetching: React.PropTypes.bool.isRequired,
  creating: React.PropTypes.bool,
};

const formConfig = {
  mapPropsToFields(props) {
    if (!props.project) {
      return {};
    }
    console.log('formConfig', props.project);

    const { project } = props;
    const values = Object.keys(project)
      .reduce((previous, current) => (
        { ...previous, [current]: { value: project[current] } }
      ), {});
    return values;
  },
};

const WrappedForm = AntForm.create(formConfig)(Form);

export default WrappedForm;
