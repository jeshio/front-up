import React from 'react';
import { Modal, Icon, Button, Table as AntTable } from 'antd';
import { Link } from 'react-router-dom';
import { PATH_PROJECTS } from '../constants';

const confirm = Modal.confirm;

const Table = function (props) {
  const onDelete = (e, project) => {
    e.preventDefault();
    confirm({
      title: `Вы уверены, что хотите удалить проект "${project.title}"?`,
      onOk() {
        props.onDelete(project);
      },
      okText: 'Да',
      cancelText: 'Отмена',
    });
  };
  const columns = [
    {
      title: 'Превью',
      dataIndex: 'preview',
      key: 'preview',
      width: 150,
      render: (text, project) => (
        <Link to={`${PATH_PROJECTS}${project.id}`}>
          <img src={project.logo_path} width="180" alt="превью проекта" />
        </Link>
      ),
    },
    {
      title: 'Заголовок',
      dataIndex: 'title',
      key: 'title',
      width: 200,
      sorter: (a, b) => (a > b ? 1 : -1),
      render: (text, project) => <Link to={`${PATH_PROJECTS}${project.id}`}>{text}</Link>,
    },
    {
      title: 'Подзаголовок',
      dataIndex: 'subtitle',
      key: 'subtitle',
      width: 200,
      sorter: (a, b) => (a > b ? 1 : -1),
    },
    {
      title: 'Срок выполнения',
      dataIndex: 'deadline',
      key: 'deadline',
      width: 150,
      sorter: (a, b) => (a > b ? 1 : -1),
    },
    {
      title: 'Дата добавления',
      dataIndex: 'created_at',
      key: 'created_at',
      sorter: (a, b) => (a > b ? 1 : -1),
    },
    {
      title: 'Действия',
      key: 'action',
      width: 250,
      render: (text, project) => (
        <span>
          {/* {
            project.public ?
              (<a href="#">Показать</a>) :
              (<a href="#">Скрыть</a>)
          }
          <span className="ant-divider" /> */}
          <Button onClick={e => props.onUpPriority(project)}><Icon type="up" /></Button>
          <span className="ant-divider" />
          <Button onClick={e => props.onDownPriority(project)}><Icon type="down" /></Button>
          <span className="ant-divider" />
          <Link to={`/projects/update/${project.id}`}>Изменить</Link>
          <span className="ant-divider" />
          <a href="#" onClick={e => onDelete(e, project)}>Удалить</a>
        </span>
      ),
    },
  ];
  const data = props.projects.map(project => ({ ...project, key: project.id }));

  return (
    <AntTable columns={columns} dataSource={data} {...props} />
  );
};

Table.propTypes = {
  projects: React.PropTypes.array.isRequired,
  onDelete: React.PropTypes.func.isRequired,
  onUpPriority: React.PropTypes.func.isRequired,
  onDownPriority: React.PropTypes.func.isRequired,
};

export default Table;
