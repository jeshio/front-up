import { Route, Switch } from 'react-router-dom';
import { List, Add, Show, Update } from './containers';

const Project = () => (
  <Switch>
    <Route path="/projects" exact component={List} />
    <Route path="/projects/add" exact component={Add} />
    <Route path="/projects/:id" exact component={Show} />
    <Route path="/projects/update/:id" exact component={Update} />
  </Switch>
);

export default Project;
