import axios from 'axios';
import { notification } from 'antd';
import * as actionTypes from './actionTypes';
import { PATH_PROJECTS_API } from './constants';

const getErrorMessage = errors => errors.response.data.message;

const toastError = description => {
  const message = 'Неполадки';
  notification.error({
    message,
    description,
  });
};

export const requestToUpPriority = project => ({
  type: actionTypes.REQUEST_TO_UP_PRIORITY,
  project,
});

export const requestToUpPrioritySuccess = () => ({
  type: actionTypes.REQUEST_TO_UP_PRIORITY_SUCCESS,
});

export const requestToDownPriority = project => ({
  type: actionTypes.REQUEST_TO_DOWN_PRIORITY,
  project,
});

export const requestToDownPrioritySuccess = () => ({
  type: actionTypes.REQUEST_TO_DOWN_PRIORITY_SUCCESS,
});

export const requestProjects = () => ({
  type: actionTypes.REQUEST_PROJECTS,
});

export const receiveProjects = json => ({
  type: actionTypes.RECEIVE_PROJECTS,
  projects: json,
  receivedAt: Date.now(),
});

export const requestProject = () => ({
  type: actionTypes.REQUEST_PROJECT,
});

export const receiveProject = json => ({
  type: actionTypes.RECEIVE_PROJECT,
  project: json,
  receivedAt: Date.now(),
});

export const requestProjectCreate = project => ({
  type: actionTypes.REQUEST_PROJECT_CREATE,
  project,
});

export const requestProjectCreateError = error => ({
  type: actionTypes.REQUEST_PROJECT_CREATE_ERROR,
  error,
});

export const requestProjectCreateSuccess = project => ({
  type: actionTypes.REQUEST_PROJECT_CREATE_SUCCESS,
  project,
});

export const requestProjectUpdate = project => ({
  type: actionTypes.REQUEST_PROJECT_UPDATE,
  project,
});

export const requestProjectUpdateError = error => ({
  type: actionTypes.REQUEST_PROJECT_UPDATE_ERROR,
  error,
});

export const requestProjectUpdateSuccess = project => ({
  type: actionTypes.REQUEST_PROJECT_UPDATE_SUCCESS,
  project,
});

export const requestProjectDelete = project => ({
  type: actionTypes.REQUEST_PROJECT_DELETE,
  project,
});

export const requestProjectDeleteError = error => ({
  type: actionTypes.REQUEST_PROJECT_DELETE_ERROR,
  error,
});

export const requestProjectDeleteSuccess = id => ({
  type: actionTypes.REQUEST_PROJECT_DELETE_SUCCESS,
  id,
});

export const fetchProjects = () => dispatch => {
  dispatch(requestProjects());
  return fetch(PATH_PROJECTS_API)
    .then(response => response.json())
    .then(json => dispatch(receiveProjects(json.data)));
};

export const fetchProject = id => dispatch => {
  dispatch(requestProject());
  return fetch(`${PATH_PROJECTS_API}${id}`)
    .then(response => response.json())
    .then(json => dispatch(receiveProject(json.data)))
    .catch(error => console.log(error));
};

export const createProject = (project, afterSuccess = () => {}) => dispatch => {
  dispatch(requestProjectCreate(project));
  return axios.post(PATH_PROJECTS_API, project)
    .then(response => response.data.data)
    .then(json => {
      dispatch(requestProjectCreateSuccess(json));
      afterSuccess(json);
    })
    .catch(errors => {
      const message = getErrorMessage(errors);
      dispatch(requestProjectCreateError(message));
      toastError(message);
    });
};

export const updateProject = (project, afterSuccess = () => {}) => dispatch => {
  dispatch(requestProjectUpdate(project));
  return axios.put(`${PATH_PROJECTS_API}${project.id}`, project)
    .then(response => response.data.data)
    .then(json => {
      dispatch(requestProjectUpdateSuccess(json));
      afterSuccess(json);
    })
    .catch(errors => {
      const message = getErrorMessage(errors);
      dispatch(requestProjectUpdateError(message));
      toastError(message);
    });
};

export const deleteProject = (project, afterSuccess = () => {}) => dispatch => {
  dispatch(requestProjectDelete(project));
  const id = project.id;

  return axios.delete(`${PATH_PROJECTS_API}${id}`)
    .then(response => response)
    .then(() => {
      dispatch(requestProjectDeleteSuccess(id));
      afterSuccess(id);
    })
    .catch(errors => {
      const message = getErrorMessage(errors);
      dispatch(requestProjectDeleteError(message));
      toastError(message);
    });
};

export const upProjectPriority = (project, afterSuccess = () => {}) => dispatch => {
  dispatch(requestToUpPriority(project));
  return axios.post(`${PATH_PROJECTS_API}up_priority/${project.id}`)
    .then(response => response.data.data)
    .then(json => {
      dispatch(requestToUpPrioritySuccess(json));
      dispatch(fetchProjects());
      afterSuccess(json);
    })
    .catch(errors => {
      const message = getErrorMessage(errors);
      toastError(message);
    });
};

export const downProjectPriority = (project, afterSuccess = () => {}) => dispatch => {
  dispatch(requestToDownPriority(project));
  return axios.post(`${PATH_PROJECTS_API}down_priority/${project.id}`)
    .then(response => response.data.data)
    .then(json => {
      dispatch(requestToDownPrioritySuccess(json));
      dispatch(fetchProjects());
      afterSuccess(json);
    })
    .catch(errors => {
      const message = getErrorMessage(errors);
      toastError(message);
    });
};
