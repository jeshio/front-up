import { selectors } from 'redux-clerk';

const ProjectsSelectors = selectors({
  baseSelector: state => state.projects,
});

export default ProjectsSelectors;
