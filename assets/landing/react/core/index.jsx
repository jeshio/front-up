import { NAME } from './constants';
import { renderComponent } from './helpers';
import { ReduxToastr } from './containers';

export const render = store => {
  renderComponent('#toastr', ReduxToastr, store);
};

export * as constants from './constants';
export reducer from './reducer';

/*
  #toastr
    Оболочка для всплывающих алертов
*/
