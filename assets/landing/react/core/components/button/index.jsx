export default class Button extends React.Component {
  static propTypes = {
    children: React.PropTypes.any.isRequired,
    onClick: React.PropTypes.func.isRequired,
    faIconClass: React.PropTypes.string,
    style: React.PropTypes.object,
    otherProps: React.PropTypes.object,
  };

  static defaultProps = {
    faIconClass: '',
    style: {
      paddingRight: '50px',
      paddingLeft: '50px',
    },
    otherProps: {},
  };

  onClick(e) {
    e.preventDefault();
    this.props.onClick();
  }

  render() {
    return (
      <a
        href="#"
        className="btn btn-solid"
        onClick={this.onClick}
        style={this.props.style}
        {...this.props.otherProps}
      >
        <span className={`fa ${this.props.faIconClass}`} /> {this.props.children}
      </a>
    );
  }
}
