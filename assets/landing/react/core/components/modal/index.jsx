import Button from '../button';

export default class Modal extends React.Component {
  static propTypes = {
    children: React.PropTypes.any.isRequired,
    modalId: React.PropTypes.string.isRequired,
  };

  static defaultProps = {
    faIconClass: '',
  };

  onClick() {
    return {};
  }

  render() {
    return (
      <div id={this.props.modalId} class="modal fade" role="dialog">
        <div className="modal-dialog">

          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal"></button>
              <Button
                {...this.props}
                otherProps={{
                  'data-dismiss': 'modal',
                }}
              >
              &times;
            </Button>
              <h4 className="modal-title">Modal Header</h4>
            </div>
            <div className="modal-body">
              {this.props.children}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
    );
  }
}
