import Button from '../button';

export default class ModalButton extends React.Component {
  static propTypes = {
    text: React.PropTypes.string.isRequired,
    faIconClass: React.PropTypes.string,
  };

  static defaultProps = {
    faIconClass: '',
  };

  onClick() {
    return {};
  }

  render() {
    const otherProps = {
      'data-toggle': 'modal',
    };
    return (
      <span>
        <Button
          {...this.props}
          otherProps={otherProps}
        />
      </span>
    );
  }
}
