import * as constants from './actionTypes';

export function addErrors(componentTitle, errors) {
  return {
    type: constants.ADD_ERRORS,
    error: { componentTitle, errors },
  };
}
