import { toastr } from 'react-redux-toastr';
import { ROOT_URL } from './constants';

// отображает компонент в элементы по селектору
export const renderComponent = (selector, Component, store, props) => {
  const elements = $(selector);

  if (elements.length > 0) {
    elements.each(index => {
      const dataset = Object.assign({}, elements[index].dataset);
      const keys = Object.keys(dataset);
      for (let i = 0; i < keys.length; i += 1) {
        try {
          const json = JSON.parse(dataset[keys[i]]);
          dataset[keys[i]] = json;
        }
        catch (e) {
          switch (dataset[keys[i]]) {
          case 'true':
            dataset[keys[i]] = true;
            break;
          case 'false':
            dataset[keys[i]] = false;
            break;
          default:
            break;
          }
        }
      }

      ReactDOM.render(
        <Component {...dataset} {...props} store={store} />,
        elements[index],
      );
    });
  }
};

export const getUrlParams = query => {
  if (!query) {
    return { };
  }

  return (/^[?#]/.test(query) ? query.slice(1) : query)
    .split('&')
    .reduce((params, param) => {
      const [key, value] = param.split('=');
      params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
      return params;
    }, { });
};

export const toastErrors = errors => {
  const title = 'Неполадки';
  if (errors === Object(errors)) {
    const errorKeys = Object.keys(errors);
    const jsxErrors = [];
    for (let i = 0; i < errorKeys.length; i += 1) {
      const error = errors[errorKeys[i]];
      if (error.length > 0) {
        jsxErrors.push(<li key={i}>- {error}</li>);
      }
    }
    toastr.error(
      title,
      {
        component: () => <ul>{jsxErrors}</ul>,
      }
    );
  }
  else {
    toastr.error(title, errors);
  }
};

export const url = needUrl => `${ROOT_URL}${needUrl}`;
