import ReactReduxToastr from 'react-redux-toastr';
import { connect, Provider } from 'react-redux';

class ReduxToastr extends React.Component {
  render() {
    return (
      <Provider {...this.props}>
        <ReactReduxToastr
          timeOut={4000}
          newestOnTop={false}
          preventDuplicates
          position="top-left"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
        />
      </Provider>
    );
  }
}

const mapStateToProps = (state, ownProps) => state.toJS();

const mapDispatchToProps = (dispatch, ownProps) => Object.assign({}, {});

export default connect()(ReduxToastr);
