import { Map, List } from 'immutable';
import * as t from './actionTypes';

const init = Map({
  errors: List(),
});

const reducer = (state = init, action) => {
  switch (action.type) {
  case t.ADD_ERRORS:
    return state
      .set('errors', state.get('errors').push(action.errors));
  default:
    return state;
  }
};

export default reducer;
