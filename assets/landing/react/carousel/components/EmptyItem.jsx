import React from 'react';

const EmptyItem = props => {
  const blockStyle = {
    padding: '5px',
    height: '354.2px',
  };
  const innerStyle = {
    padding: '15px',
    position: 'relative',
    border: '1px solid #f0f0f0',
    backgroundColor: '#fafafa',
    height: '100%',
    width: '100%',
    display: 'table',
    color: '#999',
  };

  const contentStyle = {
    textAlign: 'center',
    border: '1px solid #f0f0f0',
    height: '100%',
    borderRadius: '10px',
    fontSize: '2em',
    verticalAlign: 'middle',
    display: 'table-cell',
  };

  const linkClick = e => { e.preventDefault(); props.onClick(); };

  return (
    <div
      className="product"
      style={blockStyle}
    >
      <a href="#" onClick={linkClick}>
        <div className="product-inner" style={innerStyle}>
          <div className="product-content" style={contentStyle}>
            {props.text}
          </div>
        </div>
      </a>
    </div>
  );
};

EmptyItem.propTypes = {
  text: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired,
};

export default EmptyItem;
