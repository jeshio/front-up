import React from 'react';
import { Button, Tooltip, OverlayTrigger, Image } from 'react-bootstrap';

const Item = props => {
  const blockStyle = {
    padding: '5px',
  };
  const innerStyle = {
    padding: '10px',
    position: 'relative',
    border: '1px solid #f0f0f0',
    backgroundColor: '#fafafa',
  };
  const imageStyle = {
    maxHeight: '165px',
    overflow: 'hidden',
    margin: '0 auto',
    verticalAlign: 'middle',
    display: 'inline',
    maxWidth: '100%',
  };
  const titleStyle = {
    color: '#000000',
    display: 'inline-block',
    maxWidth: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    margin: '10px 0 10px',
    fontSize: '20px',
    textAlign: 'left',
  };
  const descriptionStyle = {
    height: '75px',
    fontSize: '14px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    color: '#8d8d8d',
    textAlign: 'left',
  };

  const imageHelperStyle = {
    display: 'inline-block',
    height: '100%',
    verticalAlign: 'middle',
  };

  const imageWrapperStyle = {
    height: '165px',
    display: 'block',
    whiteSpace: 'nowrap',
    textAlign: 'center',
    overflow: 'hidden',
  };

  const tooltip = (
    <Tooltip id="tooltip">{props.tooltipText}</Tooltip>
  );

  const linkClick = e => { e.preventDefault(); props.onClick(); };

  return (
    <div
      className="product"
      style={blockStyle}
    >
      <div className="product-inner" style={innerStyle}>
        <a href="#" onClick={linkClick}>
          <div className="product-image-wrapper" style={{ height: '165px', background: '#efefef' }}>
            <span className="product-image" style={imageWrapperStyle}>
              <span style={imageHelperStyle} />
              <Image src={props.image_url} alt={`Проект ${props.title}`} style={imageStyle} />
            </span>
          </div>
        </a>

        <div className="product-content" style={{ textAlign: 'left' }}>
          <h3 className="product-title" style={titleStyle}><a href="#" onClick={linkClick} style={{ color: '#000000' }}>{props.title}</a></h3>

          <div className="product-description" style={descriptionStyle}>
            <p>
              {props.description}
            </p>
          </div>
          <div className="product-controls">
            <div className="product-controls-wrapper" style={{ textAlign: 'right' }}>
              <OverlayTrigger placement="bottom" overlay={tooltip}>
                <Button bsStyle="default" bsSize="small" style={{ border: '0' }} onClick={linkClick}>ПОДРОБНЕЕ</Button>
              </OverlayTrigger>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Item.propTypes = {
  image_url: React.PropTypes.string,
  title: React.PropTypes.string.isRequired,
  tooltipText: React.PropTypes.object.isRequired,
  animateDuration: React.PropTypes.number.isRequired,
  onClick: React.PropTypes.func.isRequired,
};

Item.defaultProps = {
  animateDuration: 500,
};

export default Item;
