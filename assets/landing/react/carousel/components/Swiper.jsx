import React from 'react';
import Slider from 'react-slick';
import Item from './Item';
import EmptyItem from './EmptyItem';

const NextArrow = props => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, marginRight: '-10px' }}
      onClick={onClick}
      role="button"
      tabIndex={0}
    >
      <i className="icon icon-arrows-04 highlight" style={{ marginLeft: '-5px' }} />
    </div>
  );
};

const PrevArrow = props => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, marginLeft: '-10px' }}
      onClick={onClick}
      role="button"
      tabIndex={0}
    >
      <i className="icon icon-arrows-03 highlight" style={{ marginLeft: '-5px' }} />
    </div>
  );
};

const Swiper = props => {
  const responsive = [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
  ];
  const settings = {
    accessibility: true,
    dots: false,
    infinite: true,
    speed: 1500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive,
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    autoplay: true,
    autoplaySpeed: 3500,
  };

  return (
    <Slider {...settings}>
      {props.items.map(item => (
        <div key={`item_${item.id}`}>
          <Item
            title={item.title}
            description={item.subtitle}
            image_url={item.logo_mini_path}
            tooltipText={(<span>Срок выполнения: <strong>{item.deadline}</strong></span>)}
            onClick={() => props.openModal(item)}
          />
        </div>
      ))}
    </Slider>
  );
};

Swiper.propTypes = {
  openModal: React.PropTypes.func.isRequired,
  items: React.PropTypes.array.isRequired,
  emptyClick: React.PropTypes.func.isRequired,
};

export default Swiper;
