import React from 'react';
import {
  Modal as RBModal,
  Button,
  Table,
  Image,
  Row,
  Col,
} from 'react-bootstrap';
import Lightbox from 'react-images';

const getHtml = content => ({ __html: content });

const Modal = props => {
  const { item } = props;

  const logoStyle = {
    width: '100%',
  };

  // отображает значение колонки если последнее установлено
  const rowWithValue = (title, value) => (value && value.length > 0 ? (
    <div style={{ textAlign: 'right' }}>
      <b>{title}</b>
      <br />
      {value}
    </div>
  ) : '');

  return (
    <RBModal
      show={props.visible}
      onHide={props.handleOk}
      aria-labelledby="contained-modal-title"
      bsSize="large"
    >
      <RBModal.Header closeButton>
        <RBModal.Title id="contained-modal-title" className="permian">{item.title}</RBModal.Title>
      </RBModal.Header>
      <RBModal.Body>
        <Row>
          <Col md={4}>
            <a href="#" onClick={e => { e.preventDefault(); props.openImage(); }}>
              <Image src={item.logo_path} alt={`Проект ${item.title}`} style={logoStyle} />
            </a>
            <Lightbox
              images={item.logo_path ? [{ src: item.logo_path }] : []}
              isOpen={props.imageOpened}
              onClose={props.closeImage}
              backdropClosesModal
            />
          </Col>
          <Col md={8}>
            {rowWithValue('Срок выполнения', item.deadline)}
            {rowWithValue('Добавлен в портфолио', item.created_at)}
            <hr />
            <p style={{ textAlign: 'justify' }}>
              <span dangerouslySetInnerHTML={getHtml(item.description)} />
            </p>
          </Col>
        </Row>
      </RBModal.Body>
      <RBModal.Footer>
        <Button
          onClick={props.handleOk}
          bsClass="btn btn-sm btn-success btn-solid"
        >
          Закрыть
        </Button>
      </RBModal.Footer>
    </RBModal>
  );
};

Modal.propTypes = {
  item: React.PropTypes.object.isRequired,
  visible: React.PropTypes.bool.isRequired,
  imageOpened: React.PropTypes.bool.isRequired,
  handleOk: React.PropTypes.func.isRequired,
  openImage: React.PropTypes.func.isRequired,
  closeImage: React.PropTypes.func.isRequired,
};

export default Modal;
