import { connect } from 'react-redux';
import { Swiper, Modal } from '../components';
import { openPortfolusModal, closePortfolusModal, openPortfolusImage, closePortfolusImage, fetchItems } from '../actions';
import { openBidModal } from '../../bid/actions';

class Portfolio extends React.Component {
  static propTypes = {
    openModal: React.PropTypes.func.isRequired,
    closeModal: React.PropTypes.func.isRequired,
    items: React.PropTypes.array.isRequired,
    getItems: React.PropTypes.func.isRequired,
  };
  //
  // static defaultProps = {
  //   text: 'Начать',
  //   class: 'btn btn-solid',
  //   maxWidth: false,
  //   item: '',
  //   descPlaceholder: '',
  // };

  componentDidMount() {
    this.props.getItems();
  }

  render() {
    return (
      <section id="portfolio" className="section product-section align-center dark-text animated" data-animation="fadeIn" data-duration="500">
        <Modal
          handleOk={this.props.closeModal}
          visible={this.props.portfolusModalOpened}
          item={this.props.item}
          openImage={this.props.openImage}
          closeImage={this.props.closeImage}
          imageOpened={this.props.portfolusImageOpened}
        />
        <div className="container">
          <div className="section-header">
            <h2>НАШЕ <span className="highlight">ПОРТФОЛИО</span></h2>
            <p className="sub-header">
                Эти сайты — наша работа
            </p>
          </div>
          <div className="section-content row">
            <div className="col-md-12" style={{ padding: '0 35px' }}>
              <Swiper openModal={this.props.openModal} items={this.props.items} emptyClick={this.props.openBidModal} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => state.get('carousel').toJS();

const mapDispatchToProps = (dispatch, ownProps) => Object.assign({}, {
  openBidModal: () => {
    dispatch(openBidModal('портфолио'));
  },
  openModal: item => {
    dispatch(openPortfolusModal(item));
  },
  closeModal: item => {
    dispatch(closePortfolusModal(item));
  },
  openImage: () => {
    dispatch(openPortfolusImage());
  },
  closeImage: () => {
    dispatch(closePortfolusImage());
  },
  getItems: () => {
    dispatch(fetchItems());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Portfolio);
