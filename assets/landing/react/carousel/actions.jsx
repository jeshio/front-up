import * as t from './actionTypes';
import { API_URL } from './constants';
import { toastErrors } from '../core/helpers';

export function openPortfolusModal(item) {
  return {
    type: t.PORTFOLUS_MODAL_OPEN,
    item,
  };
}

export function closePortfolusModal() {
  return {
    type: t.PORTFOLUS_MODAL_CLOSE,
  };
}

export function openPortfolusImage() {
  return {
    type: t.PORTFOLUS_IMAGE_OPEN,
  };
}

export function closePortfolusImage() {
  return {
    type: t.PORTFOLUS_IMAGE_CLOSE,
  };
}

const requestReceiveItems = () => ({
  type: t.REQUEST_RECEIVE_ITEMS,
});

const requestReceiveItemsSuccess = items => ({
  type: t.REQUEST_RECEIVE_ITEMS_SUCCESS,
  items,
});

const requestReceiveItemsFailure = error => ({
  type: t.REQUEST_RECEIVE_ITEMS_FAILURE,
  error,
});

export const fetchItems = () => dispatch => {
  dispatch(requestReceiveItems());
  return fetch(API_URL)
    .then(response => response.json())
    .then(json => dispatch(requestReceiveItemsSuccess(json.data)))
    .catch(error => {
      dispatch(requestReceiveItemsFailure(error));
      toastErrors(error);
    });
};
