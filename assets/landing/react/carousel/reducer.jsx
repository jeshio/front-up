import { fromJS } from 'immutable';
import * as t from './actionTypes';

const init = fromJS({
  portfolusModalOpened: false,
  portfolusImageOpened: false,
  item: {},
  items: [],
  loading: false,
});

const reducer = (state = init, action) => {
  switch (action.type) {
  case t.PORTFOLUS_MODAL_OPEN:
    return state
      .set('item', action.item)
      .set('portfolusModalOpened', true);
  case t.PORTFOLUS_MODAL_CLOSE:
    return state
      .set('portfolusModalOpened', false);
  case t.PORTFOLUS_IMAGE_OPEN:
    return state
      .set('portfolusImageOpened', true);
  case t.PORTFOLUS_IMAGE_CLOSE:
    return state
      .set('portfolusImageOpened', false);
  case t.REQUEST_RECEIVE_ITEMS:
    return state
      .set('loading', true)
      .set('items', []);
  case t.REQUEST_RECEIVE_ITEMS_FAILURE:
    return state
      .set('loading', false)
      .set('items', []);
  case t.REQUEST_RECEIVE_ITEMS_SUCCESS:
    return state
      .set('loading', false)
      .set('items', action.items);
  default:
    return state;
  }
};

export default reducer;
