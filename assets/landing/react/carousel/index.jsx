import { Portfolio } from './containers';
import { renderComponent } from '../core/helpers';
import { NAME } from './constants';

export const render = store => {
  renderComponent(`#${NAME}-portfolio`, Portfolio, store);
};

export * as constants from './constants';
export reducer from './reducer';
