import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import { openBidModal } from '../actions';

class BidButton extends React.Component {
  static propTypes = {
    onClick: React.PropTypes.func.isRequired,
    text: React.PropTypes.string,
    class: React.PropTypes.string,
    maxWidth: React.PropTypes.bool,
    item: React.PropTypes.string,
    descPlaceholder: React.PropTypes.string,
  };

  static defaultProps = {
    text: 'Начать',
    class: 'btn btn-solid',
    maxWidth: false,
    item: '',
    descPlaceholder: '',
  };

  render() {
    const style = {};

    if (this.props.maxWidth) {
      style.width = '100%';
    }

    return (
      <Button
        bsStyle="primary"
        bsSize="large"
        bsClass={this.props.class}
        onClick={this.props.onClick}
        style={style}
      >
        {this.props.text}
      </Button>
    );
  }
}

const mapStateToProps = (state, ownProps) => state.get('bid').toJS();

const mapDispatchToProps = (dispatch, ownProps) => Object.assign({}, {
  onClick: () => {
      dispatch(openBidModal(ownProps.item, ownProps.descPlaceholder));
  },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BidButton);
