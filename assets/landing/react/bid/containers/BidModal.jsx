import { connect } from 'react-redux';
import Button from 'react-bootstrap-button-loader';
import {
  Modal,
  HelpBlock,
  Form,
  FormGroup,
  FormControl,
  Checkbox,
  Col,
  ControlLabel,
} from 'react-bootstrap';
import { API_URL } from '../constants';
import { url } from '../../core/helpers';
import { closeBidModal, createBid, changeBid } from '../actions';

class BidModal extends React.Component {
  static propTypes = {
    bidModalOpened: React.PropTypes.bool.isRequired,
    onClose: React.PropTypes.func.isRequired,
    onCreateBid: React.PropTypes.func.isRequired,
    errors: React.PropTypes.object.isRequired,
    loading: React.PropTypes.bool.isRequired,
    descPlaceholder: React.PropTypes.string.isRequired,
    newBid: React.PropTypes.shape({
      contact: React.PropTypes.string.isRequired,
      description: React.PropTypes.string.isRequired,
      item: React.PropTypes.string.isRequired,
    }).isRequired,
  };

  state = {
    contact: this.props.newBid.contact,
    contactChanged: false,
    description: this.props.newBid.description,
    descriptionChanged: false,
    policeAccept: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.newBid !== this.props.newBid) {
      this.setState({
        contact: this.props.newBid.contact,
        description: this.props.newBid.description,
      });
    }
  }

  getContactValidationState() {
    const { contact } = this.state;
    const emailRegExp = /^.+@.+\..+$/;
    const phoneRegExp = /^[0-9-+]+$/;

    if (!this.state.contactChanged && contact.length === 0) {
      return null;
    }

    return (emailRegExp.test(contact) || (phoneRegExp.test(contact) && contact.length >= 6)) ?
      'success' :
      'error';
  }

  onClose = () => {
    this.setState({
      contactChanged: false,
      descriptionChanged: false,
    });
    this.props.onClose();
  }

  onChange = (column, value) => {
    const bid = this.props.newBid;
    bid[column] = value;
    this.props.onChangeBid(bid);
  }

  render() {
    return (
      <Modal
        show={this.props.bidModalOpened}
        onHide={this.onClose}
        container={this}
        aria-labelledby="contained-modal-title"
        bsSize="large"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title" className="permian">Подать заявку</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form horizontal>
            <FormGroup
              controlId="formHorizontalEmail"
              validationState={this.getContactValidationState()}
            >
              <Col componentClass={ControlLabel} sm={2}>
                Контакт
              </Col>
              <Col sm={10}>
                <FormControl
                  type="text"
                  value={this.props.newBid.contact}
                  onChange={e => {
                    this.setState({ contactChanged: true })
                    this.onChange('contact', e.target.value);
                  }}
                  placeholder="Адрес электронной почты или телефон"
                  disabled={this.props.loading}
                />
                <FormControl.Feedback style={{ paddingTop: '7px' }} />
                <HelpBlock>
                  {!this.props.errors.email && !this.props.errors.phone ?
                    'E-mail или номер телефона' :
                    this.props.errors.email || this.props.errors.phone}
                </HelpBlock>
              </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalDescription">
              <Col componentClass={ControlLabel} sm={2}>
                Описание
                <p
                  className="text-lowercase"
                  style={{
                      lineHeight: 1,
                      fontWeight: 500,
                      fontSize: '14px',
                      color: '#adaaaa',
                  }}
                >{this.props.descPlaceholder}</p>
              </Col>
              <Col sm={10}>
                <FormControl
                  componentClass="textarea"
                  value={this.props.newBid.description}
                  onChange={e => {
                    this.setState({ descriptionChanged: true });
                    this.onChange('description', e.target.value);
                  }}
                  placeholder={this.props.descPlaceholder}
                  disabled={this.props.loading}
                />
                <HelpBlock>
                  {!this.props.errors.description ?
                    'Минимум 15 символов' :
                    this.props.errors.description}
                </HelpBlock>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col smOffset={2} sm={10}>
                <Checkbox
                    checked={this.state.policeAccept}
                    onChange={e => {
                        this.setState({ policeAccept: e.target.checked });
                    }}
                >
                  &nbsp;Я ознакомлен и согласен с <a href="/politika_konfidentsialnosti.docx" target="_blank">политикой конфиденциальности</a>.
                </Checkbox>
              </Col>
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => this.props.onCreateBid({
              contact: this.state.contact,
              description: this.state.description,
              item: this.props.newBid.item,
            })}
            bsClass="btn btn-sm btn-success btn-solid"
            disabled={!this.state.policeAccept || this.state.contact.length === 0 && !this.state.contactChanged || !this.state.descriptionChanged || this.getContactValidationState() === 'error'}
            loading={this.props.loading}
          >
            Отправить
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = (state, ownProps) => state.get('bid').toJS();

const mapDispatchToProps = (dispatch, ownProps) => Object.assign({}, {
  onClose: () => {
    dispatch(closeBidModal());
  },
  onCreateBid: bid => {
    dispatch(createBid(url(API_URL), bid));
  },
  onChangeBid: bid => {
    dispatch(changeBid(bid));
  },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BidModal);
