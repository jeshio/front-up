import { fromJS } from 'immutable';
import * as t from './actionTypes';

const init = fromJS({
  newBid: {
    description: '',
    contact: '',
    serviceId: null,
    promoCode: null,
    priceIds: [],
    item: 'Заказ',
  },
  descPlaceholder: 'Опишите, что вы хотели бы от нас получить.',
  errors: {},
  bidModalOpened: false,
  loading: false,
});

const reducer = (state = init, action) => {
  switch (action.type) {
  case t.CHANGE_BID:
    return state
      .set('newBid', fromJS(action.bid));
  case t.BID_MODAL_OPEN: {
    const newBid = state.get('newBid');

    return state
        .set('newBid', newBid.set('item', action.item ? action.item : init.get('newBid').get('item')))
        .set('descPlaceholder', action.descPlaceholder ? action.descPlaceholder : init.get('descPlaceholder'))
        .set('bidModalOpened', true);
  }

  case t.BID_MODAL_CLOSE:
    return state
      .set('bidModalOpened', false);

  // запуск запроса на создание заявки
  case t.REQUEST_CREATE_BID: {
    return state
      .set('loading', true);
  }

  // ошибка запроса на создание заявки
  case t.REQUEST_CREATE_BID_FAILURE:
    return state
      .set('errors', action.errors)
      .set('loading', false);

  // успешный запрос на создание заявки
  case t.REQUEST_CREATE_BID_SUCCESS:
    return state
      .set('newBid', init.get('newBid'))
      .set('bidModalOpened', false)
      .set('loading', false);
  default:
    return state;
  }
};

export default reducer;
