import { toastr } from 'react-redux-toastr';
import * as constants from './constants';
import * as t from './actionTypes';
import { addErrors } from '../core/actions';
import { toastErrors } from '../core/helpers';

function requestCreateBid(bid) {
  return {
    type: t.REQUEST_CREATE_BID,
    bid,
  };
}

function requestCreateBidFailure(errors) {
  return {
    type: t.REQUEST_CREATE_BID_FAILURE,
    errors,
  };
}

function requestCreateBidSuccess(bid) {
  return {
    type: t.REQUEST_CREATE_BID_SUCCESS,
    bid,
  };
}

export function changeBid(bid) {
  return {
    type: t.CHANGE_BID,
    bid,
  };
}

export function openBidModal(item, descPlaceholder) {
  ga('send', 'event', 'Заявки', 'Открыл модалку', item);
  yaCounter46250004.reachGoal('open_bid_modal');
  return {
    type: t.BID_MODAL_OPEN,
    item,
    descPlaceholder,
  };
}

export function closeBidModal() {
  ga('send', 'event', 'Заявки', 'Закрыл модалку');
  return {
    type: t.BID_MODAL_CLOSE,
  };
}

// запрос на создание заявки
export function createBid(url, bid) {
  return dispatch => {
    dispatch(requestCreateBid(bid));

    return fetch(url, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(bid),
      credentials: 'include',
    })
      .then(response => response.json())
      .then(json => {
        if (!json.success) {
          dispatch(addErrors(constants.COMPONENT_TITLE, json.message));
          dispatch(requestCreateBidFailure(json.message));
          toastErrors(json.message);
        }
        else {
          dispatch(requestCreateBidSuccess(bid));
          toastr.success('Заявка создана!', 'Мы скоро с вами свяжемся.');
          ga('send', 'event', 'Заявки', 'Отправлена', 'Заявка из модалки');
          yaCounter46250004.reachGoal('send_bid');
        }
      })
      // обработка ошибок
      .catch(err => {
        dispatch(addErrors(constants.COMPONENT_TITLE, err));
        dispatch(requestCreateBidFailure(err));
        toastErrors(err);
      });
  };
}
