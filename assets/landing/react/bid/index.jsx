import { NAME } from './constants';
import { renderComponent } from '../core/helpers';
import { BidButton, BidModal } from './containers';

export const render = store => {
  renderComponent(`#${NAME}-modal`, BidModal, store);
  renderComponent(`.${NAME}-modal-create`, BidButton, store);
};

export * as constants from './constants';
export reducer from './reducer';

/*
  .bid-modal-create
    Кнопка для подачи заявки
*/
