import { NAME } from './constants';

export const BID_MODAL_OPEN = `${NAME}/BID_MODAL_OPEN`;
export const BID_MODAL_CLOSE = `${NAME}/BID_MODAL_CLOSE`;

export const REQUEST_CREATE_BID = `${NAME}/REQUEST_CREATE_BID`;
export const REQUEST_CREATE_BID_SUCCESS = `${NAME}/REQUEST_CREATE_BID_SUCCESS`;
export const REQUEST_CREATE_BID_FAILURE = `${NAME}/REQUEST_CREATE_BID_FAILURE`;

export const CHANGE_BID = `${NAME}/CHANGE_BID`;
