import 'babel-polyfill';
import 'isomorphic-fetch';
import reactReduxToastrStyles from 'react-redux-toastr/src/styles/index.scss';
import { createLogger } from 'redux-logger';
import { applyMiddleware, createStore } from 'redux';
import { combineReducers } from 'redux-immutable';
import { Map, Iterable } from 'immutable';
import { reducer as toastrReducer } from 'react-redux-toastr';
import thunk from 'redux-thunk';
import * as bid from './bid';
import * as carousel from './carousel';
import * as core from './core';

require('eventemitter3');

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    diff: true,
    // приспосабливаем к immutable
    stateTransformer: state =>
      (Iterable.isIterable(state) ? state.toJS() : state),
  });
  middlewares.push(logger);
}

const reducers = {
  bid: bid.reducer,
  core: core.reducer,
  carousel: carousel.reducer,
  toastr: toastrReducer,
};

const app = combineReducers(reducers);

const store = createStore(
  app,
  Map(),
  applyMiddleware(...middlewares),
);

// -- Отображение компонентов в элементы
bid.render(store);
carousel.render(store);

core.render(store);
