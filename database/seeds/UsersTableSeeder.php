<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'jeshio',
            'email' => 'jeshio@yandex.ru',
            'password' => bcrypt('ujif121794020'),
            'phone' => '8-952-700-00-70',
        ]);
        DB::table('users')->insert([
            'name' => 'public',
            'email' => 'admin@front-up.ru',
            'password' => bcrypt('$%11@#099_Tt'),
            'phone' => '8',
        ]);
    }
}
