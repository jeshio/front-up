<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToColumnsBids extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bids', function (Blueprint $table) {
            $table->string('email')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('status_comment')->nullable()->change();
            $table->integer('service_id')->unsigned()->nullable()->change();
            $table->integer('cost')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bids', function (Blueprint $table) {
            $table->string('email')->change();
            $table->string('phone')->change();
            $table->string('status_comment')->change();
            $table->integer('service_id')->unsigned()->change();
            $table->integer('cost')->unsigned()->change();
        });
    }
}
