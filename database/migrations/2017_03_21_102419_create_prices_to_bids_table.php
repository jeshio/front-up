<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePricesToBidsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices_to_bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price_id')->unsigned();
            $table->integer('bid_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('price_id')
                ->references('id')->on('prices')
                ->onDelete('cascade');
            $table
                ->foreign('bid_id')
                ->references('id')->on('bids')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices_to_bids');
    }
}
