<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatusesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->smallInteger('priority')->unsigned()->default(0);
            $table->tinyInteger('ready_percent')->unsigned();
            $table->boolean('finishable')->default(false);
            $table->integer('service_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->index('name');
            $table->index('service_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statuses');
    }
}
