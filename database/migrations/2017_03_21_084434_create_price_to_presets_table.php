<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePriceToPresetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_to_presets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('preset_id')->unsigned();
            $table->integer('price_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('preset_id')
                ->references('id')->on('presets')
                ->onDelete('cascade');
            $table
                ->foreign('price_id')
                ->references('id')->on('prices')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('price_to_presets');
    }
}
