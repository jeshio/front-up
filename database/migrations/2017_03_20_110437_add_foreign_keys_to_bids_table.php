<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bids', function (Blueprint $table) {
            // добавляем необходимые связи для таблицы
            $table
                ->foreign('status_id')
                ->references('id')->on('statuses')
                ->onDelete('set null');
            $table
                ->foreign('responsible_id')
                ->references('id')->on('users')
                ->onDelete('set null');
            $table
                ->foreign('service_id')
                ->references('id')->on('services')
                ->onDelete('cascade');
            $table
                ->foreign('promo_code_id')
                ->references('id')->on('promo_codes')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bids', function (Blueprint $table) {
            $table->dropForeign(['status_id']);
            $table->dropForeign(['responsible_id']);
            $table->dropForeign(['service_id']);
            $table->dropForeign(['promo_code_id']);
        });
    }
}
