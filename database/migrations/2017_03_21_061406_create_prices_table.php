<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePricesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('cost')->unsigned();
            $table->smallInteger('priority')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('service_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->index('name');

            $table
                ->foreign('parent_id')
                ->references('id')->on('prices')
                ->onDelete('cascade');
            $table
                ->foreign('service_id')
                ->references('id')->on('services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices');
    }
}
