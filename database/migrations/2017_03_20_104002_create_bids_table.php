<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBidsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('author_email');
            $table->integer('status_id')->unsigned()->nullable();
            $table->string('status_comment');
            $table->integer('responsible_id')->unsigned()->nullable();
            $table->integer('service_id')->unsigned();
            $table->integer('cost')->unsigned();
            $table->string('hash');
            $table->integer('promo_code_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('title');
            $table->index('author_email');
            $table->index('hash');
            $table->index('service_id');
            $table->index('responsible_id');
            $table->index('promo_code_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bids');
    }
}
