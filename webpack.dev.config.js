// КОНФИГ ДЛЯ РАЗРАБОТКИ
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = require('./webpack.base.config');

const hotServerPort = 3005;

config.module.rules.push(
  {
    test: /\.css$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        'css-loader',
        'postcss-loader',
        'resolve-url-loader',
      ],
    }),
  },
  {
    test: /\.(scss|sass)$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        'css-loader',
        'postcss-loader',
        'resolve-url-loader',
        'sass-loader',
      ],
    }),
  },
  {
    test: /\.jsx$/,
    exclude: /node_modules/,
    use: [
      `babel-loader?babelrc=false&extends=${path.join(__dirname, '/.babelrc')}`,
    ],
  }
);

config.plugins.push(
  new ExtractTextPlugin({
    filename: '[name].css',
  })
  // new webpack.HotModuleReplacementPlugin()
);

config.devServer = {
  publicPath: '/assets/',
  hot: false,
  port: hotServerPort,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
  },
};

// config.devtool = 'eval-source-map';

console.log('Webpack DEV build');

module.exports = config;
