<div>
    <h1>Заявка на {{ $bid->item }}</h1>
    <p>
        <strong>Предмет заказа:</strong> {{ $bid->item }}
    </p>
    <p>
        <strong>Контакт:</strong> {{ $bid->email ? $bid->email : $bid->phone }}
    </p>
    <p>
        <strong>Описание:</strong>
        <br />
        {{ $bid->description }}
    </p>
    <p>
        <strong>Время:</strong> {{ $bid->created_at }}
    </p>
</div>