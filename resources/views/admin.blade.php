<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <title>Админка</title>

    @if (!config('app.debug'))
  	<link rel="stylesheet" type="text/css" href="{{asset('assets/admin.css')}}" />
    @else
  	<link rel="stylesheet" type="text/css" href="http://localhost:3005/assets/admin.css" />
    @endif
  </head>
  <body>
    <div id="root">
    </div>

    @if (!config('app.debug'))
  		<script type="text/javascript" src="/assets/vendor.js"></script>
  		<script type="text/javascript" src="/assets/admin.js"></script>
  	@else
  		<script type="text/javascript" src="http://localhost:3005/assets/vendor.js"></script>
  		<script type="text/javascript" src="http://localhost:3005/assets/admin.js"></script>
  	@endif
  </body>
</html>
