<section id="products" class="section about-section align-center dark-text">
    <div class="container">

        <ul class="nav nav-tabs alt">
            <li class="active"><a href="#tab-marketing" data-toggle="tab">Продвижение</a></li>
            <li><a href="#tab-visit-site" data-toggle="tab">Сайт-визитка</a></li>
            <li><a href="#tab-site-landing" data-toggle="tab">Лендинг</a></li>
        </ul>

        <div class="tab-content alt">
            <div class="tab-pane active" id="tab-marketing">
                <div class="section-header align-center">
                    <h2><span class="highlight">3 ВАРИАНТА</span></h2>
                    <p class="sub-header animated" data-duration="700" data-animation="zoomIn">которые привлекут к вам<br />поток клиентов
                    </p>
                </div>
                <div class="section-content row animated" data-duration="700" data-delay="200" data-animation="fadeInDown">
                    <div class="col-sm-4">
                        <article class="align-center">
                            <img src="{{asset('img/offers/seo.png')}}" class="margin-bottom-10" height="80" alt="seo-оптимизация" title="Заказать оптимизацию сайта для продвижения" />
                            <span class="heading">SEO-ОПТИМИЗАЦИЯ</span>
                            <p class="thin" >Поднять сайт в топ в поисковике<br />Google или Яндекс.</p>
                        </article>
                        <!--<span class="icon icon-arrows-04"></span>-->
                    </div>
                    <div class="col-sm-4">
                        <article class="align-center">
                            <img src="{{asset('img/offers/context.png')}}" class="margin-bottom-10" height="80" alt="контекстная реклама" title="Заказать настройку РСЯ, КМС" />
                            <span class="heading">КОНТЕКСТНАЯ РЕКЛАМА</span>
                            <p class="thin" >Купить рекламу по конкретным<br />запросам в поисковиках.<br />Отобразить свою рекламу<br />у конкурента.</p>
                        </article>
                        <!--<span class="icon icon-arrows-04"></span>-->
                    </div>
                    <div class="col-sm-4">
                        <article class="align-center">
                            <img src="{{asset('img/offers/target.png')}}" class="margin-bottom-10" height="80" alt="таргетинг" title="Заказать настройку рекламы в социальных сетях" />
                            <span class="heading">ТАРГЕТИНГ ВК, ИНСТАГРАМ</span>
                            <p class="thin" >Показать рекламу потенциальным клиентам,<br />отобранным по множеству параметров<br />в социальных сетях.</p>
                        </article>
                    </div>
                </div>
                <div class="align-center animated" data-duration="700" data-delay="200" data-animation="fadeInDown">
                    <span
                        class="bid-modal-create"
                        data-text="ЗАКАЗАТЬ ПРОДВИЖЕНИЕ"
                        data-item="продукты-продвижение"
                        data-desc-placeholder="Опишите вкратце, что вы хотите продвигать, в каком регионе, и на какое количество человек рассчитываете."
                    />
                </div>
                <br/>
                <br/>
            </div>

            <div class="tab-pane" id="tab-visit-site">
                <div class="section-content row">
                    <div class="col-sm-6 animated" data-delay="200" data-duration="700" data-animation="fadeInLeft">
                        <img src="{{asset('img/offers/visit-sites.png')}}" class="img-responsive" alt="сайт-визитка" title="Заказать сайт-визитку для малого бизнеса" />
                    </div>
                    <div class="col-sm-6 animated" data-delay="200" data-duration="700" data-animation="fadeInRight">
                        <br/>
                        <article class="align-center">
                            <h3>ДЛЯ МАЛОГО <span class="highlight">БИЗНЕСА</span></h3>
                            <p class="sub-title">Проверенный способ встретить вашего клиента в Интернете</p>
                            <p>Такой сайт нужен, чтобы клиент смог вас найти, ознакомиться с вашей деятельностью и связаться с вами. Бесплатной альтернативой сайту-визитке может стать группа в социальной сети, но в любом случае, сайт смотрится более престижно, чем группа и при этом не требует больших вложений.</p>
                            <br/>
                            <span
                                class="bid-modal-create"
                                data-text="ЗАКАЗАТЬ САЙТ-ВИЗИТКУ"
                                data-item="продукты-сайт-визитка"
                                data-desc-placeholder="Опишите вкратце свой бизнес, что вы хотели бы видеть на своём сайте."
                            />
                        </article>
                    </div>
                </div>
                <br/>
                <br/>
            </div>

            <div class="tab-pane" id="tab-site-landing">
                <div class="section-content row">
                    <div class="col-sm-6 pull-right animated" data-delay="200" data-duration="700" data-animation="fadeInRight">
                        <img src="{{asset('img/offers/landing.png')}}" class="img-responsive pull-right" alt="лендинг" title="Заказать лендинг (промо-сайт)" />
                    </div>
                    <div class="col-sm-6 animated" data-delay="200" data-duration="700" data-animation="fadeInLeft">
                        <article class="align-center">
                            <h3>ЗАПУСК <span class="highlight">ПРОДАЖ</span></h3>
                            <p class="sub-title">Если вы продаёте продукт или оказываете платные услуги, то<br/> этот вариант точно для вас</p>
                            <p>Промо-сайт или лендинг для многих, будь то продажа часов или оказание услуг по маникюру, является основным источником продаж. Если вы продаёте или оказываете платные услуги и у вас до сих пор нет лендинга, значит вы теряете большую часть потенциальных клиентов.</p>
                            <br/>
                            <span
                                class="bid-modal-create"
                                data-text="ЗАКАЗАТЬ ЛЕНДИНГ"
                                data-item="продукты-лендинг"
                                data-desc-placeholder="Опишите вкратце, что вы продаёте или какую услугу предоставляете."
                            />
                        </article>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>