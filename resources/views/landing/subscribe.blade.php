<section id="newsletter" class="long-block newsletter-section light-text matrix-bg">
    <div class="container align-center">
        <div class="col-sm-12 col-lg-6 animated" data-animation="fadeInLeft" data-duration="500">
            <article>
                <h2>Бесплатная консультация</h2>
                <p class="">Поможем вам понять сложные моменты и дадим совет</p>
            </article>
        </div>
        <div class="col-sm-12 col-lg-6 animated" data-animation="fadeInRight" data-duration="500">
            <span
                class="bid-modal-create"
                data-class="btn btn-outline"
                data-text="Получить консультацию"
                data-item="консультация"
                data-desc-placeholder="Напишите, что вас интересует, задайте вопросы. Мы будем рады вам помочь."
            />
        </div>
    </div>
</section>