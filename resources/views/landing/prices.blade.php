<section id="prices" class="section product-section align-center dark-text animated" data-animation="fadeInUp" data-duration="500">
    <div class="container">
        <div class="section-header">
            <h2>ВАРИАНТЫ <span class="highlight">ВАШЕГО САЙТА</span></h2>
            <p class="sub-header">
                Представляем вам три самых популярных варианта
                <br />для малого бизнеса
            </p>
        </div>
        <div class="section-content row">

            <div class="col-sm-4">
                <div class="package-column">
                    <div class="package-title">Сайт-визитка</div>
                    <div class="package-price">
                        </span><span class="old-price">9 999</span><span class="tag">-50%</span>
                        <div class="price min">4 999 <span class="currency">₽</span></div>
                    </div>
                    <div class="package-detail">
                        <ul class="list-unstyled">
                            <li>Новостная лента</li>
                            <li>Ваши контактные данные</li>
                            <li>Описание деятельности</li>
                            <li>Небольшой каталог</li>
                        </ul>
                        <span
                                class="bid-modal-create"
                                data-class="btn btn-outline-color btn-block"
                                data-max-width="true"
                                data-text="Сделать заказ"
                                data-item="цены-сайт-визитка"
                                data-desc-placeholder="Опишите кратко свой бизнес, что вы хотели бы видеть на своём сайте."
                        />
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="package-column">
                    <div class="package-title">Лендинг</div>
                    <div class="package-price">
                        <span class="old-price">11 999</span><span class="tag">-50%</span>
                        <div class="price min">5 999<span class="currency">₽</span></div>
                    </div>
                    <div class="package-detail">
                        <ul class="list-unstyled">
                            <li>Заявки на почту</li>
                            <li>Написание текста</li>
                            <li>SEO-оптимизация</li>
                            <li>UX-дизайн</li>
                        </ul>
                        <span
                                class="bid-modal-create"
                                data-class="btn btn-outline-color btn-block"
                                data-max-width="true"
                                data-text="Сделать заказ"
                                data-item="цены-лендинг"
                                data-desc-placeholder="Опишите кратко свой бизнес, что вы хотели бы видеть на своём сайте."
                        />
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="package-column">
                    <div class="package-title">Сайт-визитка + Лендинг</div>
                    <div class="package-price">
                        <span class="old-price">13 999</span><span class="tag">-50%</span>
                        <div class="price min">6 999 <span class="currency">₽</span></div>
                    </div>
                    <div class="package-detail">
                        <ul class="list-unstyled">
                            <li><strong>Хостинг</strong> на полгода</li>
                            <li><strong>Домен .RU</strong> на год</li>
                            <li><strong>SSL-сертификат</strong> на год</li>
                            <li><strong>Поддержка</strong> на 1 месяц</li>
                        </ul>
                        <span
                                class="bid-modal-create"
                                data-class="btn btn-outline-color btn-block"
                                data-max-width="true"
                                data-text="Сделать заказ"
                                data-item="цены-сайт-визитка + лендинг"
                                data-desc-placeholder="Опишите кратко свой бизнес, что вы хотели бы видеть на своём сайте."
                        />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
