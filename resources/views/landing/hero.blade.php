<div id="home" class="static-header image-version window-height light-text hero-section clearfix matrix-bg">
    <div class="matrix-bg-2">
        <div class="matrix-bg-3">
            <div class="container">
                <div class="heading-block animated align-center permian" data-animation="fadeIn" data-duration="700" style="padding-top: 135px;">
                    <h1>Нам качественное портфолио &mdash; вам <span class="highlight"><a href="#prices">сайт</a></span> и
                        <span class="highlight"><a href="#products">продвижение</a></span> за полцены</h1>
                    <h5 style="margin-bottom: 33px;">создадим прототип и результат будет таким, каким его видите вы</h5>
                    <span class="bid-modal-create" data-item="утп" />
                </div>
                <div class="image-wrapper">
                    <div class="container">
                        <img src="{{asset('img/hero/app_block.jpg')}}" alt="Прототип сайта" class="img-responsive animated" data-animation="fadeInUp" data-delay="400" data-duration="700" title="Делаем прототип разрабатываемого сайта" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>