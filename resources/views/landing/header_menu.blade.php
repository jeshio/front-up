<!-- Header -->
<header>
    <nav class="navigation navigation-header">
        <div class="container">
            <div class="navigation-brand">
                <div class="brand-logo">
                    <a href="{{ route('home') }}" class="logo"><strong>FRONT-UP</strong></a><a href="{{ route('home') }}" class="logo logo-alt"><strong>FRONT-UP</strong></a>
                </div>
            </div>
            <button class="navigation-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navigation-navbar collapsed">
                <ul class="navigation-bar navigation-bar-left">
                    <li><a href="#home">Главное</a></li>
                    <li><a href="#portfolio">Портфолио</a></li>
                    <li><a href="#products">Продукты</a></li>
                    <li><a href="#features">Особенности</a></li>
                    <li><a href="#prices">Цены</a></li>
                    <li><a href="#contacts">Контакты</a></li>
                </ul>
                <ul class="navigation-bar navigation-bar-right">
                    <li class="featured"><span class="bid-modal-create" data-text="Заказать" data-class="btn btn-sm btn-outline-color" data-item="топ-бар" /></li>
                </ul>
            </div>
        </div>
    </nav>
</header>