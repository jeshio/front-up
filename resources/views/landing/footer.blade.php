<footer id="contacts" class="footer light-text matrix-bg">
    <div class="container">
        <div class="footer-content row">
            <div class="col-sm-4 col-xs-12">
                <div class="logo-wrapper">
                    <img width="130" style="margin-top: 30px;" src="{{asset('img/logo.png')}}" alt="Логотип компании Фронт-Ап (Front-Up)" title="Заказать продвижение, разработку сайта в Ханты-Мансийске и по России" /><strong class="heading">ООО "Фронт-Ап"</strong>
                </div>
            </div>
            <div class="col-sm-5 social-wrap col-xs-12">
                <strong class="heading">Мы в сети</strong>
                <ul class="list-inline socials">
                    <li><a href="https://vk.com/front_up" target="_blank"><img width="60" style="margin-top: 30px;" src="{{asset('img/social/vkontakte.png')}}" alt="Иконка Вконтакте" title="Наша группа Вконтакте" /></a></li>
                    <li><a href="https://www.instagram.com/frontup_hm/" target="_blank"><img width="60" style="margin-top: 30px;" src="{{asset('img/social/instagram.png')}}" alt="Иконка Инстаграма" title="Наша страница в Инстаграме" /></a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <strong class="heading">Юридическая информация</strong>
                <ul class="list-unstyled">
                    <li><span class="icon icon-chat-messages-14"></span><a href="mailto:info@front-up.ru">info@front-up.ru</a></li>
                    <li><span class="icon icon-seo-icons-34"></span>628012, Ханты-Мансийск, ул. Мира, д. 43</li>
                    <li><span class="icon icon-seo-icons-17"></span>+7-982-512-21-77</li>
                    <li>+7-952-700-00-70</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copyright">www.front-up.ru 2017. Все права защищены.</div>
</footer>