<div id="clients" class="clients-section align-center">
    <div class="container">
        <ul class="list-inline logos">
            <li><img class="animated" data-animation="fadeInDown" data-duration="500" height="40" src="{{asset('img/technologies/wordpress.png')}}" alt="WordPress логотип" title="Сайты на Wordpress" /></li>
            <li><img class="animated" data-animation="fadeInUp" data-duration="500" height="40" data-delay="200" src="{{asset('img/technologies/laravel.png')}}" alt="Laravel логотип" title="Мы используем фреймворк Laravel" /></li>
            <li><img class="animated" data-animation="fadeInDown" data-duration="500" height="40" data-delay="400" src="{{asset('img/technologies/react.png')}}" alt="React логотип" title="Для фронтэнда мы используем React" /></li>
            <li><img class="animated" data-animation="fadeInUp" data-duration="500" height="40" data-delay="600" src="{{asset('img/technologies/redux.png')}}" alt="Redux логотип" title="Для архитектуры клиентской части мы используем Redux" /></li>
        </ul>
    </div>
</div>