<section id="features" class="section align-center dark-text">
    <div class="container">
        <div>
            <h2>ПОЧЕМУ <span class="highlight">МЫ</span>?</h2>
            <p>
                Потому, что с нами вы получите следующее
            </p>
        </div>
        <div class="section-content row features-list-section">
            <div class="clearfix animated" data-duration="500" data-animation="fadeInRight">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-seo-icons-27 highlight"></i>
                        <span class="heading">Адаптивность</span>
                        <p class="">Разработанные нами сайты одинаково отображаются на компьютере, планшете и телефоне.</p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-text-hierarchy-04 highlight"></i>
                        <span class="heading">Технологичность</span>
                        <p class="">Мы используем самые инновационные технологии<br />веб-разработки.</p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-food-02 highlight"></i>
                        <span class="heading">Лёгкость</span>
                        <p class="">С нашими разработками легко работать и вы всегда сможете бесплатно проконсультироваться по ним.</p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-office-24 highlight"></i>
                        <span class="heading">Долгосрочность</span>
                        <p class="">После выполнения работы мы готовы заняться поддержкой вашего сайта (включая доработки) и его продвижением.</p>
                    </article>
                </div>
            </div>
            <div class="clearfix animated" data-duration="500" data-delay="500" data-animation="fadeInLeft">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-office-02 highlight"></i>
                        <span class="heading">Гарантия</span>
                        <p class="">В течение трёх месяцев после сдачи проекта, мы бесплатно устраним любые ошибки, возникшие по нашей вине.</p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-office-56 highlight"></i>
                        <span class="heading">Прототипирование</span>
                        <p class="">Мы можем сделать прототип сайта, чтобы вы могли увидеть как он будет выглядеть и функционировать после исполнения.</p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-arrows-27 highlight"></i>
                        <span class="heading">Не только сделаем - продвинем</span>
                        <p class="">Если вы заказали сайт у нас, мы займёмся его продвижением со скидкой.</p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-seo-icons-25 highlight"></i>
                        <span class="heading">Поддерживаемый код</span>
                        <p class="">Мы пишем код, который смогут понять в других хороших IT-компаниях, поэтому вы свободны в решениях.</p>
                    </article>
                </div>
                <div class="col-md-12 col-sm-12">
                    <span class="bid-modal-create" data-text="СВЯЗАТЬСЯ С НАМИ" data-item="особенности" />
                </div>
            </div>
        </div>
    </div>
</section>