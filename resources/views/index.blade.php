<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->

	<link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Заказать разработку и продвижение сайта | Front-Up.ru</title>
	<meta name="description" content='Мы занимаемся проектированием и разработкой сайтов для бизнеса, а также рекламой существующих сайтов: SEO-оптимизацией, настройкой РСЯ, КМС.'>
	<meta name="keywords" content="разработка сайта продвижение сео seo оптимизация заказ лендинг сайт-визитка бизнес малый сделать купить сайт рся кмс таргетинг фронт-ап компания">

	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Front-Up.ru">
	<meta property="og:title" content="Заказать разработку и продвижение сайта | Front-Up.ru">
	<meta property="og:description" content="Мы занимаемся проектированием и разработкой сайтов для бизнеса, а также рекламой существующих сайтов: SEO-оптимизацией и настройкой РСЯ,КМС.">
	<meta property="og:url" content="https://www.front-up.ru/?utm_source=repost-open-graph&utm_medium=social">
	<meta property="og:locale" content="ru_RU">
	<meta property="og:image" content="https://www.front-up.ru/img/logo_sharring.png">
	<meta property="og:image:width" content="270">
	<meta property="og:image:height" content="300">

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="Заказать разработку и продвижение сайта | Front-Up.ru">
	<meta name="twitter:description" content="Мы занимаемся проектированием и разработкой сайтов для бизнеса, а также рекламой существующих сайтов: SEO-оптимизацией и настройкой РСЯ,КМС.">
	<meta name="twitter:image:src" content="https://www.front-up.ru/img/logo_sharring.png">
	<meta name="twitter:url" content="https://www.front-up.ru/?utm_source=twitter-open-graph&utm_medium=social">
	<meta name="twitter:domain" content="front-up.ru">
	<meta name="twitter:site" content="Front-Up.ru">
	<meta name="twitter:creator" content="ООО Фронт-Ап">

	{{-- Определяем канонический URL --}}
	<link rel="canonical" href="https://www.front-up.ru" />
  @if (!config('app.debug'))
	<link rel="stylesheet" type="text/css" href="{{asset('assets/landing.css')}}" />
  @else
	<link rel="stylesheet" type="text/css" href="http://localhost:3005/assets/landing.css" />
  @endif
</head>

<body id="landing-page" class="landing-page">
	@include('analytics_tracker')
	
	@include('yandex_metric_tracker')

	<div id="toastr"></div>
	<div id="bid-modal"></div>
	<!-- Preloader -->
	<div class="preloader-mask">
		<div class="preloader"><div class="spin base_clr_brd"><div class="clip left"><div class="circle"></div></div><div class="gap"><div class="circle"></div></div><div class="clip right"><div class="circle"></div></div></div></div>
	</div>

	@include('landing.header_menu')

	@include('landing.hero')

	<div id="carousel-portfolio"></div>

	@include('landing.technologies')

	@include('landing.products')

	@include('landing.subscribe')

{{--	@include('landing.descriptions')--}}

{{--	@include('landing.variants')--}}

	@include('landing.features')

	<hr class="no-margin" />

{{--	@include('landing.subscribe')--}}

	@include('landing.prices')

{{--	@include('landing.awards')--}}

{{--	@include('landing.reviews')--}}

{{--	@include('landing.team')--}}

{{--	@include('landing.call')--}}

	@include('landing.footer')

	<div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>

	@if (!config('app.debug'))
		<script type="text/javascript" src="/assets/vendor.js"></script>
		<script type="text/javascript" src="/assets/landing.js"></script>
	@else
		<script type="text/javascript" src="http://localhost:3005/assets/vendor.js"></script>
		<script type="text/javascript" src="http://localhost:3005/assets/landing.js"></script>
	@endif

	<script type='application/ld+json'>
	{
	  "@context": "http://www.schema.org",
	  "@type": "Organization",
	  "name": "Фронт-Ап",
	  "description": "Официальный сайт компании «Фронт-Ап». Компания занимается проектированием и разработкой сайтов для бизнеса, а также рекламой существующих сайтов.",
	  "url": "https://www.front-up.ru",
	  "logo": "https://www.front-up.ru/img/logo.png",
	  "address": {
		"@type": "PostalAddress",
		"streetAddress": "ул. Мира, д.43",
		"addressLocality": "Ханты-Мансийск",
		"addressRegion": "ХМАО",
		"postalCode": "628012",
		"addressCountry": "Российская Федерация"
	  },
	  "contactPoint": {
		"@type": "ContactPoint",
		"telephone": "+7 (982) 512-2177",
		"contactType": "customer service"
	  }
	}
	 </script>

	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-593c3da48a612eb2"></script>

</body>
</html>
