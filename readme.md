# Front-Up Root
## Описание
Головной сайт компании для привлечения клиентов к заказу.

Включает в себя следующий функционал:

* Оформление заявки
* Составление цен на услуги
* Управление пресетами услуг (комплекты)
* Управление заказами и их назначение
* Управление списком выполненных работ
* Управление сервисами (Веб-разработка, ремонт, дизайн)
* Управление промо-кодами

Кроме этого, содержит компоненты для лэндинга:

* Подбор вариантов по введённому количеству денег
* Калькулятор цен с выбором услуг

## Запуск

Для запуска необходимо выполнить ряд действий.

1. В корне проекта ввести следующие команды:

```
composer update
php artisan key:generate
php artisan storage:link
yarn install
yarn run build-windows
```
2. На сервере нужно настроить слеудющие перенаправления:
```
/       -> /public
```
3. Применить миграции:
```
php artisan migrate
```