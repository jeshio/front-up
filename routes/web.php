<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin', function() {
   return view('admin');
})->name('admin')->middleware('auth.basic');

Route::get('admin/{path}', function() {
   return view('admin');
})->where('path', '.+')->middleware('auth.basic');

Route::get('/', function () {
    return view('index');
})->name('home');
