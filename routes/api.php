<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::resource('promo_codes', 'PromoCodeAPIController');

Route::resource('bids', 'BidAPIController');

Route::resource('services', 'ServiceAPIController');

Route::resource('statuses', 'StatusAPIController');

Route::resource('prices', 'PriceAPIController');

Route::resource('presets', 'PresetAPIController');

Route::resource('price_to_presets', 'PriceToPresetAPIController');

Route::resource('prices_to_bids', 'PricesToBidsAPIController');

Route::resource('users', 'UserAPIController');

Route::resource('projects', 'ProjectAPIController');

Route::post('/projects/up_priority/{id}', 'ProjectAPIController@upPriority');
Route::post('/projects/down_priority/{id}', 'ProjectAPIController@downPriority');