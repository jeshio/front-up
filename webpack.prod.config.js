// КОНФИГ ДЛЯ ПРОДА
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = require('./webpack.base.config');

config.module.rules.push(
  {
    test: /\.css$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        'css-loader?minimize',
        'postcss-loader',
        'resolve-url-loader',
      ],
    }),
  },
  {
    test: /\.(scss|sass)$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        'css-loader?minimize',
        'postcss-loader',
        'resolve-url-loader',
        'sass-loader',
      ],
    }),
  },
  {
    test: /\.jsx$/,
    exclude: /node_modules/,
    use: `babel-loader?babelrc=false&extends=${path.join(__dirname, '/.babelrc')}`,
  }
);

config.plugins.push(
  new ExtractTextPlugin({
    filename: '[name].css',
  }),
  new webpack.optimize.UglifyJsPlugin({
    minimize: true,
    sourceMap: false,
    compress: {
      screw_ie8: true,
      warnings: false,
    },
    output: {
      comments: false,
    },
  })
);

console.log('Webpack PRODUCTION build');

module.exports = config;
