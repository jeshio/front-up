// БАЗОВЫЙ КОНФИГ
const webpack = require('webpack');
const path = require('path');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

const nodeEnv = process.env.NODE_ENV ? process.env.NODE_ENV.replace(' ', '') : 'development';

module.exports = {
  context: __dirname,
  entry: {
    landing: [
      'babel-polyfill',
      './index.landing.jsx',
    ],
    admin: [
      'babel-polyfill',
      './index.admin.jsx',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'public/assets'),
    // publicPath: '/assets/builds/',
    filename: '[name].js',
  },
  resolve: {
    alias: {
      // bind to modules;
      modules: path.join(__dirname, 'node_modules'),
    },
    modules: [
      path.join(__dirname, 'node_modules'),
    ],
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new webpack.ProvidePlugin({
      moment: 'moment',
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      React: 'react',
      ReactDOM: 'react-dom',
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(nodeEnv),
      },
    }),
    new ProgressBarPlugin({
      format: '  build [:bar]  :percent (:elapsed seconds)\n:current :msg',
      clear: false,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor', // Specify the common bundle's name.
    }),
    // new Manifest(
    //   path.join(`${__dirname}/config`, 'manifest.json'),
    //   {
    //     rootAssetPath: `${__dirname}/public`,
    //   }),
    // Этот файл будет являться "корневым" index.html
    // new HtmlPlugin({
    //   chunks: ['landing', 'vendor'],
    //   filename: 'index.html',
    //   template: path.join(__dirname, 'public', '_index.html'),
    // }),
  ],
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: `babel-loader?babelrc=false&extends=${path.join(__dirname, '/.babelrc')}`,
      },
      {
        test: /\.(jpg|gif|png|woff|woff2|eot|ttf|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use:  `${require.resolve("url-loader")}?limit=1000&name=img/[name].[ext]`,
      },
    ],
  },
};
