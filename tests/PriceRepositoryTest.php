<?php

use App\Models\Price;
use App\Repositories\PriceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PriceRepositoryTest extends TestCase
{
    use MakePriceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PriceRepository
     */
    protected $priceRepo;
    protected $serviceClass;
    protected $presetClass;
    protected $priceClass;

    public function setUp()
    {
        parent::setUp();
        $this->priceRepo = App::make(PriceRepository::class);
        $this->serviceClass= new class {
            use MakeServiceTrait;
        };
        $this->priceClass = new class {
            use MakePriceTrait;
        };
        $this->presetClass = new class {
            use MakePresetTrait;
        };
    }

    /**
     * @test create
     */
    public function testCreatePrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->fakePriceData([
            'service_id' => $service->id
        ]);
        $createdPrice = $this->priceRepo->create($price);
        $createdPrice = $createdPrice->toArray();
        $this->assertArrayHasKey('id', $createdPrice);
        $this->assertNotNull($createdPrice['id'], 'Created Price must have id specified');
        $this->assertNotNull(Price::find($createdPrice['id']), 'Price with given id must be in DB');
        $this->assertModelData($price, $createdPrice);
    }

    /**
     * @test read
     */
    public function testReadPrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->makePrice([
            'service_id' => $service->id
        ]);
        $dbPrice = $this->priceRepo->find($price->id);
        $dbPrice = $dbPrice->toArray();
        $this->assertModelData($price->toArray(), $dbPrice);
    }

    /**
     * @test update
     */
    public function testUpdatePrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->makePrice([
            'service_id' => $service->id
        ]);
        $fakePrice = $this->fakePriceData([
            'service_id' => $service->id
        ]);
        $updatedPrice = $this->priceRepo->update($fakePrice, $price->id);
        $this->assertModelData($fakePrice, $updatedPrice->toArray());
        $dbPrice = $this->priceRepo->find($price->id);
        $this->assertModelData($fakePrice, $dbPrice->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->makePrice([
            'service_id' => $service->id
        ]);
        $resp = $this->priceRepo->delete($price->id);
        $this->assertTrue($resp);
        $this->assertNull(Price::find($price->id), 'Price should not exist in DB');
    }
}
