<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusApiTest extends TestCase
{
    use MakeStatusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    protected $serviceClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass = new class {
            use MakeServiceTrait;
        };
    }

    /**
     * @test
     */
    public function testCreateStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->fakeStatusData([
            'service_id' => $service->id
        ]);
        $this->json('POST', '/api/v1/statuses', $status);

        $this->assertApiResponse($status);
    }

    /**
     * @test
     */
    public function testReadStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->makeStatus([
            'service_id' => $service->id
        ]);
        $this->json('GET', '/api/v1/statuses/'.$status->id);

        $this->assertApiResponse($status->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->makeStatus([
            'service_id' => $service->id
        ]);
        $editedStatus = $this->fakeStatusData([
            'service_id' => $service->id
        ]);

        $this->json('PUT', '/api/v1/statuses/'.$status->id, $editedStatus);

        $this->assertApiResponse($editedStatus);
    }

    /**
     * @test
     */
    public function testDeleteStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->makeStatus([
            'service_id' => $service->id
        ]);
        $this->json('DELETE', '/api/v1/statuses/'.$status->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/statuses/'.$status->id);

        $this->assertResponseStatus(404);
    }
}
