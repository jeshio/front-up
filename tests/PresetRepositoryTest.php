<?php

use App\Models\Preset;
use App\Repositories\PresetRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PresetRepositoryTest extends TestCase
{
    use MakePresetTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PresetRepository
     */
    protected $presetRepo;
    protected $serviceClass;

    public function setUp()
    {
        parent::setUp();
        $this->presetRepo = App::make(PresetRepository::class);
        $this->serviceClass = new class {
            use MakeServiceTrait;
        };
    }

    /**
     * @test create
     */
    public function testCreatePreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->fakePresetData([
            'service_id' => $service->id
        ]);
        $createdPreset = $this->presetRepo->create($preset);
        $createdPreset = $createdPreset->toArray();
        $this->assertArrayHasKey('id', $createdPreset);
        $this->assertNotNull($createdPreset['id'], 'Created Preset must have id specified');
        $this->assertNotNull(Preset::find($createdPreset['id']), 'Preset with given id must be in DB');
        $this->assertModelData($preset, $createdPreset);
    }

    /**
     * @test read
     */
    public function testReadPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->makePreset([
            'service_id' => $service->id
        ]);
        $dbPreset = $this->presetRepo->find($preset->id);
        $dbPreset = $dbPreset->toArray();
        $this->assertModelData($preset->toArray(), $dbPreset);
    }

    /**
     * @test update
     */
    public function testUpdatePreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->makePreset([
            'service_id' => $service->id
        ]);
        $fakePreset = $this->fakePresetData([
            'service_id' => $service->id
        ]);
        $updatedPreset = $this->presetRepo->update($fakePreset, $preset->id);
        $this->assertModelData($fakePreset, $updatedPreset->toArray());
        $dbPreset = $this->presetRepo->find($preset->id);
        $this->assertModelData($fakePreset, $dbPreset->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->makePreset([
            'service_id' => $service->id
        ]);
        $resp = $this->presetRepo->delete($preset->id);
        $this->assertTrue($resp);
        $this->assertNull(Preset::find($preset->id), 'Preset should not exist in DB');
    }
}
