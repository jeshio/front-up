<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BidApiTest extends TestCase
{
    use MakeBidTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    protected $userClass;
    protected $statusClass;
    protected $serviceClass;
    protected $promoCodeClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass = new class {
            use MakeServiceTrait;
        };
    }

    /**
     * @test
     */
    public function testCreateBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->fakeBidData([
            'service_id' => $service->id
        ]);
        $this->json('POST', '/api/v1/bids', $bid);

        $this->assertApiResponse($bid);
    }

    /**
     * @test
     */
    public function testReadBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->makeBid([
            'service_id' => $service->id
        ]);
        $this->json('GET', '/api/v1/bids/'.$bid->id);

        $this->assertApiResponse($bid->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->makeBid([
            'service_id' => $service->id
        ]);
        $editedBid = $this->fakeBidData([
            'service_id' => $service->id
        ]);

        $this->json('PUT', '/api/v1/bids/'.$bid->id, $editedBid);

        $this->assertApiResponse($editedBid);
    }

    /**
     * @test
     */
    public function testDeleteBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->makeBid([
            'service_id' => $service->id
        ]);
        $this->json('DELETE', '/api/v1/bids/'.$bid->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bids/'.$bid->id);

        $this->assertResponseStatus(404);
    }
}
