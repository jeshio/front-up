<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PricesToBidsApiTest extends TestCase
{
    use MakePricesToBidsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    protected $serviceClass;
    protected $bidClass;
    protected $priceClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass= new class {
            use MakeServiceTrait;
        };
        $this->priceClass = new class {
            use MakePriceTrait;
        };
        $this->bidClass = new class {
            use MakeBidTrait;
        };
    }

    /**
     * @test
     */
    public function testCreatePricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->fakePricesToBidsData([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $this->json('POST', '/api/v1/prices_to_bids', $pricesToBids);

        $this->assertApiResponse($pricesToBids);
    }

    /**
     * @test
     */
    public function testReadPricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->makePricesToBids([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $this->json('GET', '/api/v1/prices_to_bids/'.$pricesToBids->id);

        $this->assertApiResponse($pricesToBids->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->makePricesToBids([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $editedPricesToBids = $this->fakePricesToBidsData([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);

        $this->json('PUT', '/api/v1/prices_to_bids/'.$pricesToBids->id, $editedPricesToBids);

        $this->assertApiResponse($editedPricesToBids);
    }

    /**
     * @test
     */
    public function testDeletePricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->makePricesToBids([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $this->json('DELETE', '/api/v1/prices_to_bids/'.$pricesToBids->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/prices_to_bids/'.$pricesToBids->id);

        $this->assertResponseStatus(404);
    }
}
