<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PriceApiTest extends TestCase
{
    use MakePriceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    protected $serviceClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass = new class {
            use MakeServiceTrait;
        };
    }

    /**
     * @test
     */
    public function testCreatePrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->fakePriceData([
            'service_id' => $service->id
        ]);
        $this->json('POST', '/api/v1/prices', $price);

        $this->assertApiResponse($price);
    }

    /**
     * @test
     */
    public function testReadPrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->makePrice([
            'service_id' => $service->id
        ]);
        $this->json('GET', '/api/v1/prices/'.$price->id);

        $this->assertApiResponse($price->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->makePrice([
            'service_id' => $service->id
        ]);
        $editedPrice = $this->fakePriceData([
            'service_id' => $service->id
        ]);

        $this->json('PUT', '/api/v1/prices/'.$price->id, $editedPrice);

        $this->assertApiResponse($editedPrice);
    }

    /**
     * @test
     */
    public function testDeletePrice()
    {
        $service = $this->serviceClass->makeService();
        $price = $this->makePrice([
            'service_id' => $service->id
        ]);
        $this->json('DELETE', '/api/v1/prices/'.$price->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/prices/'.$price->id);

        $this->assertResponseStatus(404);
    }
}
