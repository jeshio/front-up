<?php

use App\Models\Status;
use App\Repositories\StatusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusRepositoryTest extends TestCase
{
    use MakeStatusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StatusRepository
     */
    protected $statusRepo;

    protected $serviceClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass = new class {
            use MakeServiceTrait;
        };
        $this->statusRepo = App::make(StatusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->fakeStatusData([
            'service_id' => $service->id
        ]);
        $createdStatus = $this->statusRepo->create($status);
        $createdStatus = $createdStatus->toArray();
        $this->assertArrayHasKey('id', $createdStatus);
        $this->assertNotNull($createdStatus['id'], 'Created Status must have id specified');
        $this->assertNotNull(Status::find($createdStatus['id']), 'Status with given id must be in DB');
        $this->assertModelData($status, $createdStatus);
    }

    /**
     * @test read
     */
    public function testReadStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->makeStatus([
            'service_id' => $service->id
        ]);
        $dbStatus = $this->statusRepo->find($status->id);
        $dbStatus = $dbStatus->toArray();
        $this->assertModelData($status->toArray(), $dbStatus);
    }

    /**
     * @test update
     */
    public function testUpdateStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->makeStatus([
            'service_id' => $service->id
        ]);
        $fakeStatus = $this->fakeStatusData();
        $updatedStatus = $this->statusRepo->update($fakeStatus, $status->id);
        $this->assertModelData($fakeStatus, $updatedStatus->toArray());
        $dbStatus = $this->statusRepo->find($status->id);
        $this->assertModelData($fakeStatus, $dbStatus->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStatus()
    {
        $service = $this->serviceClass->makeService();
        $status = $this->makeStatus([
            'service_id' => $service->id
        ]);
        $resp = $this->statusRepo->delete($status->id);
        $this->assertTrue($resp);
        $this->assertNull(Status::find($status->id), 'Status should not exist in DB');
    }
}
