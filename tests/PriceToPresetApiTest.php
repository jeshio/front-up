<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PriceToPresetApiTest extends TestCase
{
    use MakePriceToPresetTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    protected $serviceClass;
    protected $presetClass;
    protected $priceClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass= new class {
            use MakeServiceTrait;
        };
        $this->priceClass = new class {
            use MakePriceTrait;
        };
        $this->presetClass = new class {
            use MakePresetTrait;
        };
    }

    /**
     * @test
     */
    public function testCreatePriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->fakePriceToPresetData([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $this->json('POST', '/api/v1/price_to_presets', $priceToPreset);

        $this->assertApiResponse($priceToPreset);
    }

    /**
     * @test
     */
    public function testReadPriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->makePriceToPreset([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $this->json('GET', '/api/v1/price_to_presets/'.$priceToPreset->id);

        $this->assertApiResponse($priceToPreset->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->makePriceToPreset([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $editedPriceToPreset = $this->fakePriceToPresetData([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);

        $this->json('PUT', '/api/v1/price_to_presets/'.$priceToPreset->id, $editedPriceToPreset);

        $this->assertApiResponse($editedPriceToPreset);
    }

    /**
     * @test
     */
    public function testDeletePriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->makePriceToPreset([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $this->json('DELETE', '/api/v1/price_to_presets/'.$priceToPreset->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/price_to_presets/'.$priceToPreset->id);

        $this->assertResponseStatus(404);
    }
}
