<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PresetApiTest extends TestCase
{
    use MakePresetTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    protected $serviceClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass = new class {
            use MakeServiceTrait;
        };
    }

    /**
     * @test
     */
    public function testCreatePreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->fakePresetData([
            'service_id' => $service->id
        ]);
        $this->json('POST', '/api/v1/presets', $preset);

        $this->assertApiResponse($preset);
    }

    /**
     * @test
     */
    public function testReadPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->makePreset([
            'service_id' => $service->id
        ]);
        $this->json('GET', '/api/v1/presets/'.$preset->id);

        $this->assertApiResponse($preset->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->makePreset([
            'service_id' => $service->id
        ]);
        $editedPreset = $this->fakePresetData([
            'service_id' => $service->id
        ]);

        $this->json('PUT', '/api/v1/presets/'.$preset->id, $editedPreset);

        $this->assertApiResponse($editedPreset);
    }

    /**
     * @test
     */
    public function testDeletePreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->makePreset([
            'service_id' => $service->id
        ]);
        $this->json('DELETE', '/api/v1/presets/'.$preset->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/presets/'.$preset->id);

        $this->assertResponseStatus(404);
    }
}
