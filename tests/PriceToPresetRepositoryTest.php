<?php

use App\Models\PriceToPreset;
use App\Repositories\PriceToPresetRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PriceToPresetRepositoryTest extends TestCase
{
    use MakePriceToPresetTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PriceToPresetRepository
     */
    protected $priceToPresetRepo;
    protected $serviceClass;
    protected $presetClass;
    protected $priceClass;

    public function setUp()
    {
        parent::setUp();
        $this->priceToPresetRepo = App::make(PriceToPresetRepository::class);
        $this->serviceClass= new class {
            use MakeServiceTrait;
        };
        $this->priceClass = new class {
            use MakePriceTrait;
        };
        $this->presetClass = new class {
            use MakePresetTrait;
        };
    }

    /**
     * @test create
     */
    public function testCreatePriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->fakePriceToPresetData([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $createdPriceToPreset = $this->priceToPresetRepo->create($priceToPreset);
        $createdPriceToPreset = $createdPriceToPreset->toArray();
        $this->assertArrayHasKey('id', $createdPriceToPreset);
        $this->assertNotNull($createdPriceToPreset['id'], 'Created PriceToPreset must have id specified');
        $this->assertNotNull(PriceToPreset::find($createdPriceToPreset['id']), 'PriceToPreset with given id must be in DB');
        $this->assertModelData($priceToPreset, $createdPriceToPreset);
    }

    /**
     * @test read
     */
    public function testReadPriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->makePriceToPreset([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $dbPriceToPreset = $this->priceToPresetRepo->find($priceToPreset->id);
        $dbPriceToPreset = $dbPriceToPreset->toArray();
        $this->assertModelData($priceToPreset->toArray(), $dbPriceToPreset);
    }

    /**
     * @test update
     */
    public function testUpdatePriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->makePriceToPreset([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $fakePriceToPreset = $this->fakePriceToPresetData([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $updatedPriceToPreset = $this->priceToPresetRepo->update($fakePriceToPreset, $priceToPreset->id);
        $this->assertModelData($fakePriceToPreset, $updatedPriceToPreset->toArray());
        $dbPriceToPreset = $this->priceToPresetRepo->find($priceToPreset->id);
        $this->assertModelData($fakePriceToPreset, $dbPriceToPreset->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePriceToPreset()
    {
        $service = $this->serviceClass->makeService();
        $preset = $this->presetClass->makePreset([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $priceToPreset = $this->makePriceToPreset([
            'preset_id' => $preset->id,
            'price_id' => $price->id,
        ]);
        $resp = $this->priceToPresetRepo->delete($priceToPreset->id);
        $this->assertTrue($resp);
        $this->assertNull(PriceToPreset::find($priceToPreset->id), 'PriceToPreset should not exist in DB');
    }
}
