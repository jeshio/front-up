<?php

use Faker\Factory as Faker;
use App\Models\Preset;
use App\Repositories\PresetRepository;

trait MakePresetTrait
{
    /**
     * Create fake instance of Preset and save it in database
     *
     * @param array $presetFields
     * @return Preset
     */
    public function makePreset($presetFields = [])
    {
        /** @var PresetRepository $presetRepo */
        $presetRepo = App::make(PresetRepository::class);
        $theme = $this->fakePresetData($presetFields);
        return $presetRepo->create($theme);
    }

    /**
     * Get fake instance of Preset
     *
     * @param array $presetFields
     * @return Preset
     */
    public function fakePreset($presetFields = [])
    {
        return new Preset($this->fakePresetData($presetFields));
    }

    /**
     * Get fake data of Preset
     *
     * @param array $postFields
     * @return array
     */
    public function fakePresetData($presetFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->sentence(5),
            'description' => $fake->text
        ], $presetFields);
    }
}
