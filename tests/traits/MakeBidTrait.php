<?php

use Faker\Factory as Faker;
use App\Models\Bid;
use App\Repositories\BidRepository;

trait MakeBidTrait
{

    /**
     * Create fake instance of Bid and save it in database
     *
     * @param array $bidFields
     * @return Bid
     */
    public function makeBid($bidFields = [])
    {
        /** @var BidRepository $bidRepo */
        $bidRepo = App::make(BidRepository::class);
        $theme = $this->fakeBidData($bidFields);
        return $bidRepo->create($theme);
    }

    /**
     * Get fake instance of Bid
     *
     * @param array $bidFields
     * @return Bid
     */
    public function fakeBid($bidFields = [])
    {
        return new Bid($this->fakeBidData($bidFields));
    }

    /**
     * Get fake data of Bid
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBidData($bidFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->sentence(5),
            'description' => $fake->text,
            'author_email' => $fake->email,
            'status_comment' => $fake->sentence(5),
            'cost' => $fake->randomDigitNotNull,
            'hash' => $fake->word,
        ], $bidFields);
    }
}
