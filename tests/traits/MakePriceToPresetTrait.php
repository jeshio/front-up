<?php

use Faker\Factory as Faker;
use App\Models\PriceToPreset;
use App\Repositories\PriceToPresetRepository;

trait MakePriceToPresetTrait
{
    /**
     * Create fake instance of PriceToPreset and save it in database
     *
     * @param array $priceToPresetFields
     * @return PriceToPreset
     */
    public function makePriceToPreset($priceToPresetFields = [])
    {
        /** @var PriceToPresetRepository $priceToPresetRepo */
        $priceToPresetRepo = App::make(PriceToPresetRepository::class);
        $theme = $this->fakePriceToPresetData($priceToPresetFields);
        return $priceToPresetRepo->create($theme);
    }

    /**
     * Get fake instance of PriceToPreset
     *
     * @param array $priceToPresetFields
     * @return PriceToPreset
     */
    public function fakePriceToPreset($priceToPresetFields = [])
    {
        return new PriceToPreset($this->fakePriceToPresetData($priceToPresetFields));
    }

    /**
     * Get fake data of PriceToPreset
     *
     * @param array $postFields
     * @return array
     */
    public function fakePriceToPresetData($priceToPresetFields = [])
    {
        return $priceToPresetFields;
    }
}
