<?php

use Faker\Factory as Faker;
use App\Models\PricesToBids;
use App\Repositories\PricesToBidsRepository;

trait MakePricesToBidsTrait
{
    /**
     * Create fake instance of PricesToBids and save it in database
     *
     * @param array $pricesToBidsFields
     * @return PricesToBids
     */
    public function makePricesToBids($pricesToBidsFields = [])
    {
        /** @var PricesToBidsRepository $pricesToBidsRepo */
        $pricesToBidsRepo = App::make(PricesToBidsRepository::class);
        $theme = $this->fakePricesToBidsData($pricesToBidsFields);
        return $pricesToBidsRepo->create($theme);
    }

    /**
     * Get fake instance of PricesToBids
     *
     * @param array $pricesToBidsFields
     * @return PricesToBids
     */
    public function fakePricesToBids($pricesToBidsFields = [])
    {
        return new PricesToBids($this->fakePricesToBidsData($pricesToBidsFields));
    }

    /**
     * Get fake data of PricesToBids
     *
     * @param array $postFields
     * @return array
     */
    public function fakePricesToBidsData($pricesToBidsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
        ], $pricesToBidsFields);
    }
}
