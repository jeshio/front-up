<?php

use Faker\Factory as Faker;
use App\Models\PromoCode;
use App\Repositories\PromoCodeRepository;

trait MakePromoCodeTrait
{
    /**
     * Create fake instance of PromoCode and save it in database
     *
     * @param array $promoCodeFields
     * @return PromoCode
     */
    public function makePromoCode($promoCodeFields = [])
    {
        /** @var PromoCodeRepository $promoCodeRepo */
        $promoCodeRepo = App::make(PromoCodeRepository::class);
        $theme = $this->fakePromoCodeData($promoCodeFields);
        return $promoCodeRepo->create($theme);
    }

    /**
     * Get fake instance of PromoCode
     *
     * @param array $promoCodeFields
     * @return PromoCode
     */
    public function fakePromoCode($promoCodeFields = [])
    {
        return new PromoCode($this->fakePromoCodeData($promoCodeFields));
    }

    /**
     * Get fake data of PromoCode
     *
     * @param array $postFields
     * @return array
     */
    public function fakePromoCodeData($promoCodeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->sentence(2),
            'description' => $fake->text,
            'code' => $fake->text(20),
            'percentage' => $fake->numberBetween(0, 100),
            'sum' => $fake->numberBetween(0, 10000),
            'date_start' => \Carbon\Carbon::createFromTimestamp(
                $fake->dateTimeBetween('-30 days', '-20 days')->getTimestamp()
            )->subDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::createFromTimestamp(
                $fake->dateTimeBetween('-19 days', '+20 days')->getTimestamp()
            )->subDay()->format('Y-m-d')
        ], $promoCodeFields);
    }
}
