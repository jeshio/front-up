<?php

use Faker\Factory as Faker;
use App\Models\Status;
use App\Repositories\StatusRepository;

trait MakeStatusTrait
{
    use MakeServiceTrait;
    /**
     * Create fake instance of Status and save it in database
     *
     * @param array $statusFields
     * @return Status
     */
    public function makeStatus($statusFields = [])
    {
        /** @var StatusRepository $statusRepo */
        $statusRepo = App::make(StatusRepository::class);
        $theme = $this->fakeStatusData($statusFields);
        return $statusRepo->create($theme);
    }

    /**
     * Get fake instance of Status
     *
     * @param array $statusFields
     * @return Status
     */
    public function fakeStatus($statusFields = [])
    {
        return new Status($this->fakeStatusData($statusFields));
    }

    /**
     * Get fake data of Status
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStatusData($statusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->sentence(5),
            'priority' => $fake->numberBetween(0, 63999),
            'ready_percent' => $fake->numberBetween(0, 100),
            'finishable' => rand(0,1) > 0
        ], $statusFields);
    }
}
