<?php

use App\Models\PricesToBids;
use App\Repositories\PricesToBidsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PricesToBidsRepositoryTest extends TestCase
{
    use MakePricesToBidsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PricesToBidsRepository
     */
    protected $pricesToBidsRepo;
    protected $serviceClass;
    protected $bidClass;
    protected $priceClass;

    public function setUp()
    {
        parent::setUp();
        $this->pricesToBidsRepo = App::make(PricesToBidsRepository::class);
        $this->serviceClass= new class {
            use MakeServiceTrait;
        };
        $this->priceClass = new class {
            use MakePriceTrait;
        };
        $this->bidClass = new class {
            use MakeBidTrait;
        };
    }

    /**
     * @test create
     */
    public function testCreatePricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->fakePricesToBidsData([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $createdPricesToBids = $this->pricesToBidsRepo->create($pricesToBids);
        $createdPricesToBids = $createdPricesToBids->toArray();
        $this->assertArrayHasKey('id', $createdPricesToBids);
        $this->assertNotNull($createdPricesToBids['id'], 'Created PricesToBids must have id specified');
        $this->assertNotNull(PricesToBids::find($createdPricesToBids['id']), 'PricesToBids with given id must be in DB');
        $this->assertModelData($pricesToBids, $createdPricesToBids);
    }

    /**
     * @test read
     */
    public function testReadPricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->makePricesToBids([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $dbPricesToBids = $this->pricesToBidsRepo->find($pricesToBids->id);
        $dbPricesToBids = $dbPricesToBids->toArray();
        $this->assertModelData($pricesToBids->toArray(), $dbPricesToBids);
    }

    /**
     * @test update
     */
    public function testUpdatePricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->makePricesToBids([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $fakePricesToBids = $this->fakePricesToBidsData([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $updatedPricesToBids = $this->pricesToBidsRepo->update($fakePricesToBids, $pricesToBids->id);
        $this->assertModelData($fakePricesToBids, $updatedPricesToBids->toArray());
        $dbPricesToBids = $this->pricesToBidsRepo->find($pricesToBids->id);
        $this->assertModelData($fakePricesToBids, $dbPricesToBids->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePricesToBids()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->bidClass->makeBid([
            'service_id' => $service->id,
        ]);
        $price = $this->priceClass->makePrice([
            'service_id' => $service->id,
        ]);
        $pricesToBids = $this->makePricesToBids([
            'bid_id' => $bid->id,
            'price_id' => $price->id,
        ]);
        $resp = $this->pricesToBidsRepo->delete($pricesToBids->id);
        $this->assertTrue($resp);
        $this->assertNull(PricesToBids::find($pricesToBids->id), 'PricesToBids should not exist in DB');
    }
}
