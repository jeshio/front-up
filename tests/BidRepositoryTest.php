<?php

use App\Models\Bid;
use App\Repositories\BidRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BidRepositoryTest extends TestCase
{
    use MakeBidTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BidRepository
     */
    protected $bidRepo;
    protected $serviceClass;

    public function setUp()
    {
        parent::setUp();
        $this->serviceClass = new class {
            use MakeServiceTrait;
        };
        $this->bidRepo = App::make(BidRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->fakeBidData([
            'service_id' => $service->id
        ]);
        $createdBid = $this->bidRepo->create($bid);
        $createdBid = $createdBid->toArray();
        $this->assertArrayHasKey('id', $createdBid);
        $this->assertNotNull($createdBid['id'], 'Created Bid must have id specified');
        $this->assertNotNull(Bid::find($createdBid['id']), 'Bid with given id must be in DB');
        $this->assertModelData($bid, $createdBid);
    }

    /**
     * @test read
     */
    public function testReadBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->makeBid([
            'service_id' => $service->id
        ]);
        $dbBid = $this->bidRepo->find($bid->id);
        $dbBid = $dbBid->toArray();
        $this->assertModelData($bid->toArray(), $dbBid);
    }

    /**
     * @test update
     */
    public function testUpdateBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->makeBid([
            'service_id' => $service->id
        ]);
        $fakeBid = $this->fakeBidData([
            'service_id' => $service->id
        ]);
        $updatedBid = $this->bidRepo->update($fakeBid, $bid->id);
        $this->assertModelData($fakeBid, $updatedBid->toArray());
        $dbBid = $this->bidRepo->find($bid->id);
        $this->assertModelData($fakeBid, $dbBid->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBid()
    {
        $service = $this->serviceClass->makeService();
        $bid = $this->makeBid([
            'service_id' => $service->id
        ]);
        $resp = $this->bidRepo->delete($bid->id);
        $this->assertTrue($resp);
        $this->assertNull(Bid::find($bid->id), 'Bid should not exist in DB');
    }
}
