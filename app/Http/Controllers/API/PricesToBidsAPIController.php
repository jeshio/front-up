<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePricesToBidsAPIRequest;
use App\Http\Requests\API\UpdatePricesToBidsAPIRequest;
use App\Models\PricesToBids;
use App\Repositories\PricesToBidsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PricesToBidsController
 * @package App\Http\Controllers\API
 */

class PricesToBidsAPIController extends AppBaseController
{
    /** @var  PricesToBidsRepository */
    private $pricesToBidsRepository;

    public function __construct(PricesToBidsRepository $pricesToBidsRepo)
    {
        $this->pricesToBidsRepository = $pricesToBidsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/prices_to_bids",
     *      summary="Get a listing of the PricesToBids.",
     *      tags={"PricesToBids"},
     *      description="Get all PricesToBids",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PricesToBids")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->pricesToBidsRepository->pushCriteria(new RequestCriteria($request));
        $this->pricesToBidsRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pricesToBids = $this->pricesToBidsRepository->all();

        return $this->sendResponse($pricesToBids->toArray(), 'Prices To Bids retrieved successfully');
    }

    /**
     * @param CreatePricesToBidsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/prices_to_bids",
     *      summary="Store a newly created PricesToBids in storage",
     *      tags={"PricesToBids"},
     *      description="Store PricesToBids",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PricesToBids that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PricesToBids")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PricesToBids"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePricesToBidsAPIRequest $request)
    {
        $input = $request->all();

        $pricesToBids = $this->pricesToBidsRepository->create($input);

        return $this->sendResponse($pricesToBids->toArray(), 'Prices To Bids saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/prices_to_bids/{id}",
     *      summary="Display the specified PricesToBids",
     *      tags={"PricesToBids"},
     *      description="Get PricesToBids",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PricesToBids",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PricesToBids"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PricesToBids $pricesToBids */
        $pricesToBids = $this->pricesToBidsRepository->findWithoutFail($id);

        if (empty($pricesToBids)) {
            return $this->sendError('Prices To Bids not found');
        }

        return $this->sendResponse($pricesToBids->toArray(), 'Prices To Bids retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePricesToBidsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/prices_to_bids/{id}",
     *      summary="Update the specified PricesToBids in storage",
     *      tags={"PricesToBids"},
     *      description="Update PricesToBids",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PricesToBids",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PricesToBids that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PricesToBids")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PricesToBids"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePricesToBidsAPIRequest $request)
    {
        $input = $request->all();

        /** @var PricesToBids $pricesToBids */
        $pricesToBids = $this->pricesToBidsRepository->findWithoutFail($id);

        if (empty($pricesToBids)) {
            return $this->sendError('Prices To Bids not found');
        }

        $pricesToBids = $this->pricesToBidsRepository->update($input, $id);

        return $this->sendResponse($pricesToBids->toArray(), 'PricesToBids updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/prices_to_bids/{id}",
     *      summary="Remove the specified PricesToBids from storage",
     *      tags={"PricesToBids"},
     *      description="Delete PricesToBids",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PricesToBids",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PricesToBids $pricesToBids */
        $pricesToBids = $this->pricesToBidsRepository->findWithoutFail($id);

        if (empty($pricesToBids)) {
            return $this->sendError('Prices To Bids not found');
        }

        $pricesToBids->delete();

        return $this->sendResponse($id, 'Prices To Bids deleted successfully');
    }
}
