<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePromoCodeAPIRequest;
use App\Http\Requests\API\UpdatePromoCodeAPIRequest;
use App\Models\PromoCode;
use App\Repositories\PromoCodeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PromoCodeController
 * @package App\Http\Controllers\API
 */

class PromoCodeAPIController extends AppBaseController
{
    /** @var  PromoCodeRepository */
    private $promoCodeRepository;

    public function __construct(PromoCodeRepository $promoCodeRepo)
    {
        $this->promoCodeRepository = $promoCodeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/promo_codes",
     *      summary="Get a listing of the PromoCodes.",
     *      tags={"PromoCode"},
     *      description="Get all PromoCodes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PromoCode")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->promoCodeRepository->pushCriteria(new RequestCriteria($request));
        $this->promoCodeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $promoCodes = $this->promoCodeRepository->all();

        return $this->sendResponse($promoCodes->toArray(), 'Promo Codes retrieved successfully');
    }

    /**
     * @param CreatePromoCodeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/promo_codes",
     *      summary="Store a newly created PromoCode in storage",
     *      tags={"PromoCode"},
     *      description="Store PromoCode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PromoCode that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PromoCode")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PromoCode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePromoCodeAPIRequest $request)
    {
        $input = $request->all();

        $promoCodes = $this->promoCodeRepository->create($input);

        return $this->sendResponse($promoCodes->toArray(), 'Promo Code saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/promo_codes/{id}",
     *      summary="Display the specified PromoCode",
     *      tags={"PromoCode"},
     *      description="Get PromoCode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PromoCode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PromoCode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PromoCode $promoCode */
        $promoCode = $this->promoCodeRepository->findWithoutFail($id);

        if (empty($promoCode)) {
            return $this->sendError('Promo Code not found');
        }

        return $this->sendResponse($promoCode->toArray(), 'Promo Code retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePromoCodeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/promo_codes/{id}",
     *      summary="Update the specified PromoCode in storage",
     *      tags={"PromoCode"},
     *      description="Update PromoCode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PromoCode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PromoCode that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PromoCode")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PromoCode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePromoCodeAPIRequest $request)
    {
        $input = $request->all();

        /** @var PromoCode $promoCode */
        $promoCode = $this->promoCodeRepository->findWithoutFail($id);

        if (empty($promoCode)) {
            return $this->sendError('Promo Code not found');
        }

        $promoCode = $this->promoCodeRepository->update($input, $id);

        return $this->sendResponse($promoCode->toArray(), 'PromoCode updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/promo_codes/{id}",
     *      summary="Remove the specified PromoCode from storage",
     *      tags={"PromoCode"},
     *      description="Delete PromoCode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PromoCode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PromoCode $promoCode */
        $promoCode = $this->promoCodeRepository->findWithoutFail($id);

        if (empty($promoCode)) {
            return $this->sendError('Promo Code not found');
        }

        $promoCode->delete();

        return $this->sendResponse($id, 'Promo Code deleted successfully');
    }
}
