<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBidAPIRequest;
use App\Http\Requests\API\UpdateBidAPIRequest;
use App\Models\Bid;
use App\Repositories\BidRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BidController
 * @package App\Http\Controllers\API
 */

class BidAPIController extends AppBaseController
{
    /** @var  BidRepository */
    private $bidRepository;

    public function __construct(BidRepository $bidRepo)
    {
        $this->bidRepository = $bidRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/bids",
     *      summary="Get a listing of the Bids.",
     *      tags={"Bid"},
     *      description="Get all Bids",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Bid")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->bidRepository->pushCriteria(new RequestCriteria($request));
        $this->bidRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bids = $this->bidRepository->all();

        return $this->sendResponse($bids->toArray(), 'Bids retrieved successfully');
    }

    /**
     * @param CreateBidAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/bids",
     *      summary="Store a newly created Bid in storage",
     *      tags={"Bid"},
     *      description="Store Bid",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bid that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bid")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bid"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBidAPIRequest $request)
    {
        $input = $request->all();

        $bids = $this->bidRepository->create($input);

        return $this->sendResponse($bids->toArray(), 'Bid saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/bids/{id}",
     *      summary="Display the specified Bid",
     *      tags={"Bid"},
     *      description="Get Bid",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bid",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bid"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Bid $bid */
        $bid = $this->bidRepository->findWithoutFail($id);

        if (empty($bid)) {
            return $this->sendError('Bid not found');
        }

        return $this->sendResponse($bid->toArray(), 'Bid retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBidAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bids/{id}",
     *      summary="Update the specified Bid in storage",
     *      tags={"Bid"},
     *      description="Update Bid",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bid",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bid that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bid")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bid"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBidAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bid $bid */
        $bid = $this->bidRepository->findWithoutFail($id);

        if (empty($bid)) {
            return $this->sendError('Bid not found');
        }

        $bid = $this->bidRepository->update($input, $id);

        return $this->sendResponse($bid->toArray(), 'Bid updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bids/{id}",
     *      summary="Remove the specified Bid from storage",
     *      tags={"Bid"},
     *      description="Delete Bid",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bid",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Bid $bid */
        $bid = $this->bidRepository->findWithoutFail($id);

        if (empty($bid)) {
            return $this->sendError('Bid not found');
        }

        $bid->delete();

        return $this->sendResponse($id, 'Bid deleted successfully');
    }
}
