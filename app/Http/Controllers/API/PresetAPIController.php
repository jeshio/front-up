<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePresetAPIRequest;
use App\Http\Requests\API\UpdatePresetAPIRequest;
use App\Models\Preset;
use App\Repositories\PresetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PresetController
 * @package App\Http\Controllers\API
 */

class PresetAPIController extends AppBaseController
{
    /** @var  PresetRepository */
    private $presetRepository;

    public function __construct(PresetRepository $presetRepo)
    {
        $this->presetRepository = $presetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/presets",
     *      summary="Get a listing of the Presets.",
     *      tags={"Preset"},
     *      description="Get all Presets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Preset")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->presetRepository->pushCriteria(new RequestCriteria($request));
        $this->presetRepository->pushCriteria(new LimitOffsetCriteria($request));
        $presets = $this->presetRepository->all();

        return $this->sendResponse($presets->toArray(), 'Presets retrieved successfully');
    }

    /**
     * @param CreatePresetAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/presets",
     *      summary="Store a newly created Preset in storage",
     *      tags={"Preset"},
     *      description="Store Preset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Preset that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Preset")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Preset"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePresetAPIRequest $request)
    {
        $input = $request->all();

        $presets = $this->presetRepository->create($input);

        return $this->sendResponse($presets->toArray(), 'Preset saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/presets/{id}",
     *      summary="Display the specified Preset",
     *      tags={"Preset"},
     *      description="Get Preset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Preset",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Preset"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Preset $preset */
        $preset = $this->presetRepository->findWithoutFail($id);

        if (empty($preset)) {
            return $this->sendError('Preset not found');
        }

        return $this->sendResponse($preset->toArray(), 'Preset retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePresetAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/presets/{id}",
     *      summary="Update the specified Preset in storage",
     *      tags={"Preset"},
     *      description="Update Preset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Preset",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Preset that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Preset")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Preset"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePresetAPIRequest $request)
    {
        $input = $request->all();

        /** @var Preset $preset */
        $preset = $this->presetRepository->findWithoutFail($id);

        if (empty($preset)) {
            return $this->sendError('Preset not found');
        }

        $preset = $this->presetRepository->update($input, $id);

        return $this->sendResponse($preset->toArray(), 'Preset updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/presets/{id}",
     *      summary="Remove the specified Preset from storage",
     *      tags={"Preset"},
     *      description="Delete Preset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Preset",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Preset $preset */
        $preset = $this->presetRepository->findWithoutFail($id);

        if (empty($preset)) {
            return $this->sendError('Preset not found');
        }

        $preset->delete();

        return $this->sendResponse($id, 'Preset deleted successfully');
    }
}
