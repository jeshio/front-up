<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePriceToPresetAPIRequest;
use App\Http\Requests\API\UpdatePriceToPresetAPIRequest;
use App\Models\PriceToPreset;
use App\Repositories\PriceToPresetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PriceToPresetController
 * @package App\Http\Controllers\API
 */

class PriceToPresetAPIController extends AppBaseController
{
    /** @var  PriceToPresetRepository */
    private $priceToPresetRepository;

    public function __construct(PriceToPresetRepository $priceToPresetRepo)
    {
        $this->priceToPresetRepository = $priceToPresetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/price_to_presets",
     *      summary="Get a listing of the PriceToPresets.",
     *      tags={"PriceToPreset"},
     *      description="Get all PriceToPresets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PriceToPreset")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->priceToPresetRepository->pushCriteria(new RequestCriteria($request));
        $this->priceToPresetRepository->pushCriteria(new LimitOffsetCriteria($request));
        $priceToPresets = $this->priceToPresetRepository->all();

        return $this->sendResponse($priceToPresets->toArray(), 'Price To Presets retrieved successfully');
    }

    /**
     * @param CreatePriceToPresetAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/price_to_presets",
     *      summary="Store a newly created PriceToPreset in storage",
     *      tags={"PriceToPreset"},
     *      description="Store PriceToPreset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PriceToPreset that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PriceToPreset")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PriceToPreset"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePriceToPresetAPIRequest $request)
    {
        $input = $request->all();

        $priceToPresets = $this->priceToPresetRepository->create($input);

        return $this->sendResponse($priceToPresets->toArray(), 'Price To Preset saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/price_to_presets/{id}",
     *      summary="Display the specified PriceToPreset",
     *      tags={"PriceToPreset"},
     *      description="Get PriceToPreset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PriceToPreset",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PriceToPreset"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PriceToPreset $priceToPreset */
        $priceToPreset = $this->priceToPresetRepository->findWithoutFail($id);

        if (empty($priceToPreset)) {
            return $this->sendError('Price To Preset not found');
        }

        return $this->sendResponse($priceToPreset->toArray(), 'Price To Preset retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePriceToPresetAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/price_to_presets/{id}",
     *      summary="Update the specified PriceToPreset in storage",
     *      tags={"PriceToPreset"},
     *      description="Update PriceToPreset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PriceToPreset",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PriceToPreset that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PriceToPreset")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PriceToPreset"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePriceToPresetAPIRequest $request)
    {
        $input = $request->all();

        /** @var PriceToPreset $priceToPreset */
        $priceToPreset = $this->priceToPresetRepository->findWithoutFail($id);

        if (empty($priceToPreset)) {
            return $this->sendError('Price To Preset not found');
        }

        $priceToPreset = $this->priceToPresetRepository->update($input, $id);

        return $this->sendResponse($priceToPreset->toArray(), 'PriceToPreset updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/price_to_presets/{id}",
     *      summary="Remove the specified PriceToPreset from storage",
     *      tags={"PriceToPreset"},
     *      description="Delete PriceToPreset",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PriceToPreset",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PriceToPreset $priceToPreset */
        $priceToPreset = $this->priceToPresetRepository->findWithoutFail($id);

        if (empty($priceToPreset)) {
            return $this->sendError('Price To Preset not found');
        }

        $priceToPreset->delete();

        return $this->sendResponse($id, 'Price To Preset deleted successfully');
    }
}
