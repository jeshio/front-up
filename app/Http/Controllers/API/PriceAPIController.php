<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePriceAPIRequest;
use App\Http\Requests\API\UpdatePriceAPIRequest;
use App\Models\Price;
use App\Repositories\PriceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PriceController
 * @package App\Http\Controllers\API
 */

class PriceAPIController extends AppBaseController
{
    /** @var  PriceRepository */
    private $priceRepository;

    public function __construct(PriceRepository $priceRepo)
    {
        $this->priceRepository = $priceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/prices",
     *      summary="Get a listing of the Prices.",
     *      tags={"Price"},
     *      description="Get all Prices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Price")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->priceRepository->pushCriteria(new RequestCriteria($request));
        $this->priceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $prices = $this->priceRepository->all();

        return $this->sendResponse($prices->toArray(), 'Prices retrieved successfully');
    }

    /**
     * @param CreatePriceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/prices",
     *      summary="Store a newly created Price in storage",
     *      tags={"Price"},
     *      description="Store Price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Price that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Price")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Price"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePriceAPIRequest $request)
    {
        $input = $request->all();

        $prices = $this->priceRepository->create($input);

        return $this->sendResponse($prices->toArray(), 'Price saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/prices/{id}",
     *      summary="Display the specified Price",
     *      tags={"Price"},
     *      description="Get Price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Price",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Price"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Price $price */
        $price = $this->priceRepository->findWithoutFail($id);

        if (empty($price)) {
            return $this->sendError('Price not found');
        }

        return $this->sendResponse($price->toArray(), 'Price retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePriceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/prices/{id}",
     *      summary="Update the specified Price in storage",
     *      tags={"Price"},
     *      description="Update Price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Price",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Price that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Price")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Price"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePriceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Price $price */
        $price = $this->priceRepository->findWithoutFail($id);

        if (empty($price)) {
            return $this->sendError('Price not found');
        }

        $price = $this->priceRepository->update($input, $id);

        return $this->sendResponse($price->toArray(), 'Price updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/prices/{id}",
     *      summary="Remove the specified Price from storage",
     *      tags={"Price"},
     *      description="Delete Price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Price",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Price $price */
        $price = $this->priceRepository->findWithoutFail($id);

        if (empty($price)) {
            return $this->sendError('Price not found');
        }

        $price->delete();

        return $this->sendResponse($id, 'Price deleted successfully');
    }
}
