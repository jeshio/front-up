<?php

namespace App\Http\Requests\API;

use App\Http\Requests\APIRequestWithErrorsFormat;
use App\Models\Bid;

class CreateBidAPIRequest extends APIRequestWithErrorsFormat
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Bid::$rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required_without_all' =>
              trans('validation.required', [
                'attribute' => trans('validation.attributes.contact')
              ]),
            'phone.required_without_all' =>
              '',
        ];
    }

    /**
     * Validate the input.
     *
     * @param  \Illuminate\Validation\Factory  $factory
     * @return \Illuminate\Validation\Validator
     */
    public function validator($factory)
    {
        return $factory->make(
            $this->sanitizeInput(), $this->container->call([$this, 'rules']), $this->messages()
        );
    }

    /**
     * Sanitize the input.
     *
     * @return array
     */
    protected function sanitizeInput()
    {
        if (method_exists($this, 'sanitize'))
        {
            return $this->container->call([$this, 'sanitize']);
        }

        return $this->all();
    }

    /**
     * Sanitize input before validation.
     *
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();
        $contact = $input['contact'];

        $email_reg_exp = '/@/';

        if (preg_match($email_reg_exp, $contact) > 0)
            $input['email'] = $contact;
        else
            $input['phone'] = $contact;

        $this->replace($input);

        return $this->all();
    }
}
