<?php

namespace App\Http\Requests\API;

use App\Models\Project;
use InfyOm\Generator\Request\APIRequest;

class CreateProjectAPIRequest extends APIRequest
{
    private static function upload($base64, $name) {
        $image = base64_decode($base64);

        \Storage::disk('local')->put($image, $name);
    }

    private static function getBase64($file) {
        try {
            $logoBase64withScheme = $file['url'];
            return explode(',', $logoBase64withScheme)[1];
        }
        catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        $creatingRules = [
            'logo' => 'required',
            'extension' => 'required',
        ];

        return Project::$rules + $creatingRules;
    }

    public function sanitize()
    {
        $input = $this->all();

        try {
            $logo = $input['logo'];

            $base64 = explode(',', $logo)[1];

            $input['logo'] = base64_decode($base64);
        }
        catch (\ErrorException $e) {

        }

        $this->replace($input);
    }
}
