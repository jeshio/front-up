<?php

namespace App\Http\Requests\API;

use App\Models\Project;
use InfyOm\Generator\Request\APIRequest;

class UpdateProjectAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         $this->sanitize();

         return Project::$rules;
     }

     public function sanitize()
     {
         $input = $this->all();

         try {
             $logo = $input['logo'];

             $base64 = explode(',', $logo)[1];

             $input['logo'] = base64_decode($base64);
         }
         catch (\ErrorException $e) {

         }

         $this->replace($input);
     }
}
