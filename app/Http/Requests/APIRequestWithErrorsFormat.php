<?php

/**
 * Created by PhpStorm.
 * User: ga_ivanov
 * Date: 19.04.2017
 * Time: 12:03
 */

namespace App\Http\Requests;
use Illuminate\Http\JsonResponse;
use InfyOm\Generator\Request\APIRequest;
use InfyOm\Generator\Utils\ResponseUtil;

class APIRequestWithErrorsFormat extends APIRequest
{
    /**
     * Get the failed validation response for the request.
     *
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors)
    {
        $messages = [];

        foreach ($errors as $field => $message) {
            $messages[$field] = $message[0];
        }

        return response()->json(ResponseUtil::makeError($messages),
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
