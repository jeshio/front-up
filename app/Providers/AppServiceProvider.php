<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // dev зависимости
        if (!$this->app->environment('production')) {
            $this->app->register('Jlapp\Swaggervel\SwaggervelServiceProvider');
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
            $this->app->register('Collective\Html\HtmlServiceProvider');
            $this->app->register('Laracasts\Flash\FlashServiceProvider');
            $this->app->register('Prettus\Repository\Providers\RepositoryServiceProvider');
            $this->app->register('\InfyOm\AdminLTETemplates\AdminLTETemplatesServiceProvider');
        }
    }
}
