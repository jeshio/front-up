<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;

/**
 * @SWG\Definition(
 *      definition="Project",
 *      required={"title", "description", "deadline"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtitle",
 *          description="subtitle",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="deadline",
 *          description="deadline",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="public",
 *          description="public",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Project extends Model
{
    use SoftDeletes;

    public $table = 'projects';
    protected $appends = ['logo_path', 'logo_mini_path', 'extension', 'logo_base64'];
    protected $hidden = ['logo_base64'];
    protected $logo;
    protected $extension;

    static public $imageMiniWidth = 350;

    protected $dates = ['deleted_at'];

    private static function upload($base64, $name, $extension, $exists = false)
    {
        $disk = 'local';
        \Log::info('upload FILE '.$name);

        if ($exists) {
            \Log::info('upload FILE EXISTS');
            \Storage::delete($name);
        }

        // загружаем миниатюру
        $imgMini = Image::make($base64)->widen(self::$imageMiniWidth, function ($constraint) {
            $constraint->upsize();
        })->encode('jpg', 60);
        \Storage::disk($disk)->put($name.'_'.self::$imageMiniWidth.'.jpg', $imgMini);

        // обрабатываем само изображение
        $img = Image::make($base64)->encode('jpg', 85);

        return \Storage::disk($disk)->put($name.'.jpg', $img);
    }

    public $fillable = [
        'title',
        'subtitle',
        'description',
        'deadline',
        'public',
        'priority',
     ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'subtitle' => 'string',
        'description' => 'string',
        'deadline' => 'string',
        'public' => 'boolean',
        'priority' => 'integer',
    ];

    public static function boot() {
        parent::boot();

        self::creating(function($model){
            $maxPriority = Project::max('priority');

            if ($maxPriority)
                $model->priority = $maxPriority + 1;
            else
                $model->priority = 1;
        });

        self::created(function($model){
            self::upload($model->logo, self::getLogoPath($model), $model->extension);
        });

        self::updated(function($model){
            if ($model->logo)
                self::upload($model->logo, self::getLogoPath($model), $model->extension, $model->getLogoPathAttribute());
        });

        // после удаления проекта, удаляем его логотип
        self::deleting(function($model){
            $logoPath = $model->getLogoAbsolutePath();
            if ($logoPath) {
                \Log::info($logoPath);
                $logoMiniPath = $model->getLogoAbsolutePath(true);
                \Storage::delete([$logoPath, $logoMiniPath]);
            }
        });
    }

    static function getLogoPath($model)
    {
        return "public/projects/previews/{$model->id}";
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required',
        'deadline' => 'required'
    ];

    public function setPublic($value)
    {
        $this->public = !isEmpty($value);
    }

    public function getLogoAbsolutePath($mini = false)
    {
        // находим название файла, исходя из id
        $allFiles = \Storage::files('public/projects/previews');

        $matchingFiles = preg_grep("/\/({$this->id}".($mini ? '_'.self::$imageMiniWidth : '')."\.[^\/]+)$/", $allFiles);

        try {
            if (count($matchingFiles) === 0)
                throw new \ErrorException();
            $file = reset($matchingFiles);

            return $file;
        }
        catch (\ErrorException $e)
        {

        }

        return null;
    }

    public function getDescriptionAttribute($value)
    {
        return \Html::decode($value);
    }

    public function getLogoPathAttribute()
    {
        $absolutePath = $this->getLogoAbsolutePath();

        return $absolutePath ? preg_replace("/^public(.+)/", "/storage$1", $absolutePath) : null;
    }

    public function getLogoMiniPathAttribute()
    {
        $absolutePath = $this->getLogoAbsolutePath(true);

        return $absolutePath ? preg_replace("/^public(.+)/", "/storage$1", $absolutePath) : null;
    }

    public function  getLogoBase64Attribute()
    {
        $path = $this->getLogoPathAttribute();
        if (!$path)
            return null;

        $path = ltrim($path, '/storage/');
        $logoPath = \Storage::get("public/{$path}");

        if (!$logoPath)
            return null;

        $type = pathinfo($path, PATHINFO_EXTENSION);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($logoPath);

        return $base64;
    }

    public function getExtensionAttribute()
    {
        $path = $this->getLogoPathAttribute();
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        return $extension;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($value)
    {
        $this->logo = $value;
    }

    public function getExtension()
    {
        return $this->extension;
    }

    public function setExtension($value)
    {
        $this->extension = $value;
    }

    public function fill(array $attributes)
    {
        // добавляю аттрибуты, которые не запишутся в БД
        if (isset($attributes['logo']))
        {
            $this->setLogo($attributes['logo']);
        }

        if (isset($attributes['extension']))
        {
            $this->setExtension($attributes['extension']);
        }

        return parent::fill($attributes);
    }

}
