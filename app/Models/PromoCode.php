<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="PromoCode",
 *      required={"name", "code", "date_start", "date_end"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="percentage",
 *          description="percentage",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sum",
 *          description="sum",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="date_start",
 *          description="date_start",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="date_end",
 *          description="date_end",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class PromoCode extends Model
{
    use SoftDeletes;

    public $table = 'promo_codes';
    

    protected $dates = [
        'date_start',
        'date_end',
        'deleted_at'
    ];

    // формат для вывода в JSON интервала дат
    protected $rangeDateFormat = "Y-m-d";

    public $fillable = [
        'name',
        'description',
        'code',
        'percentage',
        'sum',
        'date_start',
        'date_end'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'code' => 'string',
        'percentage' => 'integer',
        'sum' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'code' => 'required|min:3|max:20',
        'percentage' => 'integer|min:0|max:100',
        'sum' => 'integer|min:0',
        'date_start' => 'required|date_format:Y-m-d',
        'date_end' => 'required|date_format:Y-m-d|after:date_start'
    ];

    public function getDateStartAttribute( $value ) {
        return (new Carbon($value))->format($this->rangeDateFormat);
    }

    public function getDateEndAttribute( $value ) {
        return (new Carbon($value))->format($this->rangeDateFormat);
    }
    
}
