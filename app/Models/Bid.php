<?php

namespace App\Models;

use App\Mail\BidShipped;
use Eloquent as Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Bid",
 *      required={"title", "description", "author_email", "cost"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status_id",
 *          description="status_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status_comment",
 *          description="status_comment",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="responsible_id",
 *          description="responsible_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="service_id",
 *          description="service_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cost",
 *          description="cost",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="hash",
 *          description="hash",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="item",
 *          description="интересующий предмет к заказу",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="promo_code_id",
 *          description="promo_code_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Bid extends Model
{
    use SoftDeletes;

    public $table = 'bids';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'description',
        'email',
        'phone',
        'status_id',
        'status_comment',
        'responsible_id',
        'service_id',
        'item',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'description' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'status_id' => 'integer',
        'status_comment' => 'string',
        'responsible_id' => 'integer',
        'service_id' => 'integer',
        'cost' => 'integer',
        'hash' => 'string',
        'promo_code_id' => 'integer',
        'item' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'description' => 'required|min:15',
        'email' => 'required_without_all:phone|email',
        'phone' => 'required_without_all:email|min:6',
        'status_comment' => 'min:3',
        'status_id' => 'integer|min:1',
        'responsible_id' => 'integer|min:1',
        'service_id' => 'integer|min:1',
        'promo_code_id' => 'integer|min:1',
        'cost' => 'integer|min:0'
    ];

    public static function sendToCRM($bid)
    {
        $client = new Client();
        $fields = [
            'TITLE' => $bid->phone ? $bid->phone : $bid->email,
            'HAS_PHONE' => !empty($bid->phone),
            'HAS_EMAIL' => !empty($bid->email),
            'COMMENTS' => $bid->description,
            'SOURCE_DESCRIPTION' => $bid->item,
            'OPENED' => 'Y',
            'ASSIGNED_BY_ID' => env('BITRIX_USER_ASSIGNED_BIDS_ID'),
        ];

        if ($bid->phone)
        {
            $fields['PHONE'] = [["VALUE" => $bid->phone]];
        }

        if ($bid->email)
        {
            $fields['EMAIL'] = [["VALUE" => $bid->email]];
        }

        $client->post('https://front-up.bitrix24.ru/rest/6/v7n63zfa4779z24b/crm.lead.add', [
            'json' => [
                'fields' => $fields,
                'params' => [
                    'REGISTER_SONET_EVENT' => 'Y',
                ]
            ]
        ]);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($bid) {

            $hash = bcrypt("{$bid->email}{$bid->email}{time()}");

            $bid->hash = $hash;
        });

        // после создания заявки отправляем её на почту
        static::created(function($bid){
            static::sendToCRM($bid);

            \Mail::to(env('BIDS_MAIL'))
                ->queue(new BidShipped($bid));
        });
    }

}
