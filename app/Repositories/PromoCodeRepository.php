<?php

namespace App\Repositories;

use App\Models\PromoCode;
use InfyOm\Generator\Common\BaseRepository;

class PromoCodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'code',
        'percentage',
        'sum',
        'date_start',
        'date_end'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromoCode::class;
    }
}
