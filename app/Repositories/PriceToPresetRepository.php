<?php

namespace App\Repositories;

use App\Models\PriceToPreset;
use InfyOm\Generator\Common\BaseRepository;

class PriceToPresetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'preset_id',
        'price_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PriceToPreset::class;
    }
}
