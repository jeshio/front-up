<?php

namespace App\Repositories;

use App\Models\PricesToBids;
use InfyOm\Generator\Common\BaseRepository;

class PricesToBidsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'price_id',
        'bid_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PricesToBids::class;
    }
}
