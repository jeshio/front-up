<?php

namespace App\Repositories;

use App\Models\Bid;
use InfyOm\Generator\Common\BaseRepository;

class BidRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'email',
        'phone',
        'status_id',
        'status_comment',
        'responsible_id',
        'service_id',
        'cost',
        'hash',
        'promo_code_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bid::class;
    }
}
