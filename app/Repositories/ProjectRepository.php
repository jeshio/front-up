<?php

namespace App\Repositories;

use App\Models\Project;
use InfyOm\Generator\Common\BaseRepository;

class ProjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'subtitle',
        'description',
        'deadline',
        'public'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Project::class;
    }

    public function upPriority($id)
    {
        $project = $this->findWithoutFail($id);

        $upperProject = Project::orderBy('priority')->whereRaw("priority > {$project->priority}", 'id')->first();

        if ($upperProject) {
            $oldPriority = $project->priority;
            $project->priority = $upperProject->priority;
            $upperProject->priority = $oldPriority;

            $project->save();
            $upperProject->save();
        }

        return $project;
    }

    public function downPriority($id)
    {
        $project = $this->findWithoutFail($id);

        $downProject = Project::orderBy('priority', 'desc')->whereRaw("priority < {$project->priority}", 'id')->first();

        if ($downProject) {
            $oldPriority = $project->priority;
            $project->priority = $downProject->priority;
            $downProject->priority = $oldPriority;

            $project->save();
            $downProject->save();
        }

        return $project;
    }
}
