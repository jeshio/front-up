<?php

namespace App\Repositories;

use App\Models\Price;
use InfyOm\Generator\Common\BaseRepository;

class PriceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'cost',
        'priority',
        'parent_id',
        'service_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Price::class;
    }
}
