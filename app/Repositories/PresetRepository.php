<?php

namespace App\Repositories;

use App\Models\Preset;
use InfyOm\Generator\Common\BaseRepository;

class PresetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'service_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Preset::class;
    }
}
